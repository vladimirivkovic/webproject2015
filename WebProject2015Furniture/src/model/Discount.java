package model;

import java.util.Date;

public class Discount {
	static private int seed = 0;
	public static int getSeed() {
		return seed;
	}

	public static void setSeed(int seed) {
		Discount.seed = seed;
	}

	private int id;
	private Date start;
	private Date finish;
	private double percent;
	private int it;
	private FurnitureItem item;
	private FurnitureType type;
	private FurnitureShowroom showroom;
	
	public Discount() {
		// TODO Auto-generated constructor stub
	}

	public Discount(Date start, Date finish, double percent, int it,
			FurnitureItem item, FurnitureType type, FurnitureShowroom showroom) {
		super();
		this.id = getNextId();
		this.start = start;
		this.finish = finish;
		this.percent = percent;
		this.it = it;
		this.item = item;
		this.type = type;
		this.showroom = showroom;
		
		if(this.it == 0) {
			this.type = null;
			this.showroom = this.item.getShowroom();
		} else {
			this.item = null;
		}
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getStart() {
		return start;
	}

	public void setStart(Date start) {
		this.start = start;
	}

	public Date getFinish() {
		return finish;
	}

	public void setFinish(Date finish) {
		this.finish = finish;
	}

	public double getPercent() {
		return percent;
	}

	public void setPercent(double percent) {
		this.percent = percent;
	}

	public int getIt() {
		return it;
	}

	public void setIt(int it) {
		this.it = it;
	}

	public FurnitureItem getItem() {
		return item;
	}

	public void setItem(FurnitureItem item) {
		this.item = item;
	}

	public FurnitureType getType() {
		return type;
	}

	public void setType(FurnitureType type) {
		this.type = type;
	}

	public FurnitureShowroom getShowroom() {
		return showroom;
	}

	public void setShowroom(FurnitureShowroom showroom) {
		this.showroom = showroom;
	}
	
	public static int getNextId() {
		return seed++;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj == null)
			return false;
		if(obj instanceof Discount) {
			return ((Discount) obj).getId() == this.getId();
		} else {
			return false;
		}
	}
}
