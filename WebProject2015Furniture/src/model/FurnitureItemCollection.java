package model;

import java.util.ArrayList;
import java.util.HashMap;

public class FurnitureItemCollection {
	private HashMap<String, FurnitureItem> items;
	
	public FurnitureItemCollection() {
		items = new HashMap<String, FurnitureItem>();
	}

	public ArrayList<FurnitureItem> getItems() {
		return new ArrayList<FurnitureItem>(items.values());
	}
	
	public ArrayList<FurnitureItem> getActiveItems() {
		ArrayList<FurnitureItem> retVal = new ArrayList<FurnitureItem>();
		for(FurnitureItem item : items.values()) {
			if(!item.isDeleted()) {
				retVal.add(item);
			}
		}
		
		return retVal;
	}
	
	public FurnitureItem getItem(String id) {
		return items.get(id);
	}
	
	public FurnitureItem getActiveItem(String id) {
		FurnitureItem i = items.get(id);
		if(i == null || i.isDeleted()) {
			return null;
		}
		return i;
	}
	
	public synchronized boolean addItem(FurnitureItem fi) {
		if(items.containsKey(fi.getCode())) {
			return false;
		} else {
			items.put(fi.getCode(), fi);
			return true;
		}
	}
	
	public synchronized boolean addNewItem(FurnitureItem fi) {
		if(items.containsKey(fi.getCode())) {
			FurnitureItem i = items.get(fi.getCode());
			if(i.isDeleted()) {
				items.put(fi.getCode(), fi);
				return true;
			}
			return false;
		} else {
			items.put(fi.getCode(), fi);
			return true;
		}
	}
	
	public synchronized boolean setItem(FurnitureItem fi) {
		System.out.println(fi.getName() + " editing...");
		if(!items.containsKey(fi.getCode())) {
			return false;
		} else {
			System.out.println(fi.getName() + " editing...");
			FurnitureItem i = items.get(fi.getCode());
			if(i.isDeleted()) {
				return false;
			}
			System.out.println(fi.getName() + " editing...");
			if(fi.getImageOrVideo() == null ||
					fi.getImageOrVideo().equals(""))
				fi.setImageOrVideo(i.getImageOrVideo());
			items.put(fi.getCode(), fi);
			System.out.println(fi.getName() + " edited");
			return true;
		}
	}
	
	public synchronized boolean buyItem(String id, int count) {
		if(items.containsKey(id)) {
			FurnitureItem fi = items.get(id);
			if(fi.isDeleted()) {
				return false;
			}
			if(fi.getCount() >= count) {
				fi.setCount(fi.getCount() - count);
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
	public synchronized ArrayList<FurnitureItem> getItemsByType(FurnitureType type) {
		ArrayList<FurnitureItem> retVal = new ArrayList<FurnitureItem>();
		
		for(FurnitureItem fi : items.values()) {
			if(fi.isDeleted())
				continue;
			if(fi.getType().equals(type)) {
				retVal.add(fi);
			}
			if(fi.getType().isParent(type)) {
				retVal.add(fi);
			}
		}
		
		return retVal;
	}
	
	public synchronized ArrayList<FurnitureItem> getItemsByShowroom(String showroomId) {
		ArrayList<FurnitureItem> retVal = new ArrayList<FurnitureItem>();
		
		for(FurnitureItem fi : items.values()) {
			if(fi.isDeleted())
				continue;
			if(fi.getShowroom().getName().equals(showroomId)) {
				retVal.add(fi);
			}
		}
		
		return retVal;
	}
	
	public synchronized boolean deleteItem(String id, AdditionalServiceCollection asc) {
		if(items.containsKey(id)) {
			if(asc.getServicesByItem(id).size() > 0) {
				return false;
			}
			FurnitureItem item = items.get(id);
			item.setDeleted();
			return true;
		} else {
			return false;
		}
	}
}
