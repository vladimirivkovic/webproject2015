package model;

import java.util.ArrayList;
import java.util.HashMap;

public class ShoppingCart {
	private HashMap<String, ShoppingCartItem> items;
	
	public ShoppingCart() {
		items = new HashMap<String, ShoppingCartItem>();
	}
	
	public ArrayList<ShoppingCartItem> getItems() {
		return new ArrayList<ShoppingCartItem>(items.values());
	}
	
	public void addToShoppingCart(ShoppingCartItem item) {
		ShoppingCartItem i = items.get(item.getProduct().getCode());
		if(i == null) {
			items.put(item.getProduct().getCode(), item);
		} else {
			i.setCount(i.getCount()+1);
			i.setServices(item.getServicesMap());
		}
	}
	
	public void removeFromShoppingCart(ShoppingCartItem item) {
		items.remove(item.getProduct().getCode());
	}
	
	public double calculate() {
		double sum = 0;
		for (ShoppingCartItem i : items.values()) {
			sum += i.getProduct().getPrice()*i.getCount();
		}
		return sum;
	}
	
	public void countMinus(ShoppingCartItem item) {
		ShoppingCartItem i = items.get(item.getProduct().getCode());
		if(i != null && i.getCount() > 1) {
			i.setCount(i.getCount()-1);
		}
	}

	public void empty() {
		items.clear();
	}
	
	public ArrayList<AdditionalService> getServicesFor(String code) {
		ShoppingCartItem cartItem = items.get(code);
		if(cartItem != null) {
			return cartItem.getServices();
		} else {
			return null;
		}
	}

	public void editItem(ShoppingCartItem editedItem) {
		ShoppingCartItem i = items.get(editedItem.getProduct().getCode());
		if(i != null) {
			i.setServices(editedItem.getServicesMap());
		}
		
	}
	
	public ShoppingCartItem getItemByCode(String id) {
		return items.get(id);
	}
}
