package model;

import java.util.ArrayList;
import java.util.HashMap;

public class UsersCollection {
	private HashMap<String, User> users;
	
	public UsersCollection() {
		users = new HashMap<String, User>();
	}
	
	public User getUser(String username) {
		return users.get(username);
	}
	
	public ArrayList<User> getUsers() {
		return new ArrayList<User>(users.values());
	}
	
	public boolean addUser(User u) {
		if(!users.containsKey(u.getUsername())) {
			users.put(u.getUsername(), u);
			return true;
		} else {
			return false;
		}
	}
	
	public User validate(String username, String password) {
		for(User u : users.values()) {
			if(u.getUsername().equals(username) && u.getPassword().equals(password)) {
				return u;
			}
		}
		return null;
	}
}
