package model;

import java.util.Collection;

public class FurnitureShowroom {
	private String name;
	private String address;
	private String telephone;
	private String email;
	private String webSite;
	private String pib;
	private String mid;
	private String accountNumber;
	//private Collection<FurnitureItem> items;
	
	public FurnitureShowroom()
	{
		
	}
	
	
	public FurnitureShowroom(String name, String address, String telephone,
			String email, String pib) {
		super();
		this.name = name;
		this.address = address;
		this.telephone = telephone;
		this.email = email;
		this.pib = pib;
	}


	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getWebSite() {
		return webSite;
	}
	public void setWebSite(String webSite) {
		this.webSite = webSite;
	}
	public String getPib() {
		return pib;
	}
	public void setPib(String pib) {
		this.pib = pib;
	}
	public String getMid() {
		return mid;
	}
	public void setMid(String mid) {
		this.mid = mid;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	
	@Override
	public boolean equals(Object obj) {
		if(obj == null)
			return false;
		if(obj instanceof FurnitureShowroom) {
			return ((FurnitureShowroom) obj).getPib().equals(this.getPib());
		} else {
			return false;
		}
	}
	
	
}
