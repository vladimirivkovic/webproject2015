package model;

import java.util.ArrayList;
import java.util.HashMap;

import com.sun.org.apache.xerces.internal.impl.RevalidationHandler;

import sun.net.www.protocol.http.AuthCacheValue.Type;

public class FurnitureTypeCollection {
	private HashMap<String, FurnitureType> types;

	public FurnitureTypeCollection() {
		types = new HashMap<String, FurnitureType>();
	}

	public synchronized ArrayList<FurnitureType> getTypes() {
		ArrayList<FurnitureType> retVal = new ArrayList<FurnitureType>();
		
		for (FurnitureType ft : types.values()) {
			retVal.add(ft);
		}
		
		return retVal;
	}
	
	public synchronized ArrayList<FurnitureType> getActiveTypes() {
		ArrayList<FurnitureType> retVal = new ArrayList<FurnitureType>();
		
		for (FurnitureType ft : types.values()) {
			if(!ft.isDeleted()) {
				retVal.add(ft);
			}
		}
		
		return retVal;
	}

	public synchronized FurnitureType getType(String id) {
		FurnitureType t = types.get(id);
		if (t == null) {
			return null;
		}
		return t;
	}
	
	public synchronized FurnitureType getActiveType(String id) {
		FurnitureType t = types.get(id);
		if (t == null || t.isDeleted()) {
			return null;
		}
		return t;
	}

	public synchronized boolean addType(FurnitureType ft) {
		if (types.containsKey(ft.getName())) {
/*			FurnitureType t = types.get(ft.getName());
			if (t.isDeleted()) {
				types.put(ft.getName(), ft);
				return true;
			}*/
			return false;
		} else {
			types.put(ft.getName(), ft);
			return true;
		}
	}
	
	public synchronized boolean addNewType(FurnitureType ft) {
		if (types.containsKey(ft.getName())) {
			FurnitureType t = types.get(ft.getName());
			if (t.isDeleted()) {
				types.put(ft.getName(), ft);
				return true;
			}
			return false;
		} else {
			types.put(ft.getName(), ft);
			return true;
		}
	}

	public synchronized boolean setType(FurnitureType ft) {
		if (!types.containsKey(ft.getName())) {
			return false;
		} else {
			FurnitureType t = types.get(ft.getName());
			if (t.isDeleted() || areLinked(t, ft.getSuperType())) {
				return false;
			}
			if(ft.getSuperType() != null && ft.getName().equals(ft.getSuperType().getName())) {
				return false;
			}
			types.put(ft.getName(), ft);
			return true;
		}
	}

	public synchronized FurnitureType deleteType(String id,
			FurnitureItemCollection fic) {
		synchronized (fic) {

			if (types.containsKey(id)) {
				FurnitureType type = types.get(id);

				if (fic.getItemsByType(type).size() > 0) {
					return null;
				} else if (getSubtypesOf(type).size() > 0) {
					return null;
				} else {
					FurnitureType t = types.get(id);
					t.setDeleted();
					return t;
				}
			} else {
				return null;
			}
		}
	}
	
	public synchronized boolean areLinked(FurnitureType ancestor, FurnitureType descendant) {
		FurnitureType t = descendant;
		if(t == null || t.getSuperType() == null) {
			return false;
		}
		while(!t.getSuperType().equals(ancestor)) {
			t = t.getSuperType();
			if(t.getSuperType() == null){
				return false;
			}
		}
		return true;
	}

	public synchronized ArrayList<FurnitureType> getSubtypesOf(FurnitureType ft) {
		ArrayList<FurnitureType> retVal = new ArrayList<FurnitureType>();

		for (FurnitureType furnitureType : types.values()) {
			if(furnitureType.isDeleted())
				continue;
			if (ft == null && furnitureType.getSuperType() == ft) {
				retVal.add(furnitureType);
			}
			if (ft != null
					&& ft.equals(furnitureType.getSuperType())) {
				retVal.add(furnitureType);
			}
		}

		return retVal;
	}
}
