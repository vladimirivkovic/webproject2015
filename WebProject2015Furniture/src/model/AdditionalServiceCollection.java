package model;

import java.util.ArrayList;
import java.util.HashMap;

public class AdditionalServiceCollection {
	private HashMap<String, AdditionalService> services;
	
	public AdditionalServiceCollection() {
		services = new HashMap<String, AdditionalService>();
	}
	
	public ArrayList<AdditionalService> getServices() {
		return new ArrayList<AdditionalService>(services.values());
	}
	
	public void setServices(HashMap<String, AdditionalService> services) {
		this.services = services;
	}
	
	public AdditionalService getService(String id) {
		return services.get(id);
	}
	
	public synchronized boolean addService(AdditionalService as) {
		if(services.containsKey(as.getName())) {
			return false;
		} else {
			services.put(as.getName(), as);
			return true;
		}
	}
	
	public synchronized boolean setService(AdditionalService as) {
		if(!services.containsKey(as.getName())) {
			return false;
		} else {
			services.put(as.getName(), as);
			return true;
		}
	}
	
	public synchronized ArrayList<AdditionalService> getServicesByItem(String code) {
		ArrayList<AdditionalService> retVal = new ArrayList<AdditionalService>();
		
		for(AdditionalService ac : services.values()) {
			if(ac.getItem().getCode().equals(code)) {
				retVal.add(ac);
			}
		}
		
		return retVal;
	}
	
	public synchronized boolean deleteService(String id) {
		if(services.containsKey(id)) {
			services.remove(id);
			return true;
		} else {
			return false;
		}
	}
	
	public ArrayList<AdditionalService> getServicesFor(String code) {
		ArrayList<AdditionalService> retVal = new ArrayList<AdditionalService>();
		
		for (AdditionalService as : services.values()) {
			if(as.getItem().getCode().equals(code)) {
				retVal.add(as);
			}
		}
		
		return retVal;
	}
}
