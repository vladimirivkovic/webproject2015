package model;

public class AdditionalService {
	private String name;
	private String description;
	private double price;
	private FurnitureItem item;
	
	public AdditionalService() {
		// TODO Auto-generated constructor stub
	}

	public AdditionalService(String name, String description, double price,
			FurnitureItem item) {
		super();
		this.name = name;
		this.description = description;
		this.price = price;
		this.item = item;
	}

	public FurnitureItem getItem() {
		return item;
	}

	public void setItem(FurnitureItem item) {
		this.item = item;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj == null)
			return false;
		if(obj instanceof AdditionalService) {
			return ((AdditionalService) obj).getName().equals(this.getName());
		} else {
			return false;
		}
	}

}
