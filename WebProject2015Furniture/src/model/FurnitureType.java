package model;


public class FurnitureType {
	private String name;
	private String description;
	private FurnitureType superType;
	private boolean deleted;
	
	public FurnitureType() {
		// TODO Auto-generated constructor stub
	}

	public FurnitureType(String name, String description, FurnitureType superType) {
		super();
		this.name = name;
		this.description = description;
		this.superType = superType;
		
		this.deleted = false;
	}



	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public FurnitureType getSuperType() {
		return superType;
	}

	public void setSuperType(FurnitureType subType) {
		this.superType = subType;
	}
	
	public boolean isParent(FurnitureType type) {
		return type.equals(this.superType);
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj == null)
			return false;
		if(obj instanceof FurnitureType) {
			return ((FurnitureType) obj).getName().equals(this.getName());
		} else {
			return false;
		}
	}
	
	public void setDeleted() {
		this.deleted = true;
	}
	
	public synchronized boolean isDeleted() {
		return deleted;
	}
	
}
