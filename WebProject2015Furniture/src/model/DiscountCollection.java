package model;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import sun.misc.Perf;

public class DiscountCollection {
	private HashMap<Integer, Discount> discounts;

	public DiscountCollection() {
		discounts = new HashMap<Integer, Discount>();
	}

	public ArrayList<Discount> getDiscounts() {
		return new ArrayList<Discount>(discounts.values());
	}

	public Discount getDiscount(int id) {
		return discounts.get(id);
	}

	public synchronized boolean addDiscount(Discount d) {
		if (discounts.containsKey(d.getId())) {
			return false;
		} else {
			discounts.put(d.getId(), d);
			return true;
		}
	}

	public synchronized boolean setDiscount(Discount d) {
		if (!discounts.containsKey(d.getId())) {
			return false;
		} else {
			discounts.put(d.getId(), d);
			return true;
		}
	}

	public synchronized Discount getDiscountByItem(FurnitureItem fi) {
		Discount retVal = null;
		double max = 0;

		for (Discount d : discounts.values()) {
			if (d.getIt() == 0) {
				if (d.getItem().getCode().equals(fi.getCode())) {
					if (d.getPercent() > max) {
						max = d.getPercent();
						retVal = d;
					}
				}
			} else {
				if (fi.getType().getName().equals(d.getType().getName())) {
					if (d.getPercent() > max) {
						max = d.getPercent();
						retVal = d;
					}
				}
			}
		}

		return retVal;
	}

	public synchronized Discount getDiscountByType(String name) {
		Discount retVal = null;
		double max = 0;

		for (Discount d : discounts.values()) {
			if (d.getIt() == 1) {
				if (d.getType().getName().equals(name)) {
					if (d.getPercent() > max) {
						max = d.getPercent();
						retVal = d;
					}
				}
			}
		}

		return retVal;
	}

	public synchronized boolean deleteDiscount(int id) {
		if (discounts.containsKey(id)) {
			discounts.remove(id);
			return true;
		} else {
			return false;
		}
	}

	public double getMaxDiscountFor(FurnitureItem item,
			FurnitureTypeCollection ftc) {
		double d = 0;
		Date today = new Date();
		for (Discount discount : discounts.values()) {
			if (discount.getStart().before(today)
					&& discount.getFinish().after(today)) {
				if (discount.getIt() == 1) {
					if (ftc.areLinked(discount.getType(), item.getType())) {
						if (discount.getShowroom().equals(item.getShowroom())) {
							if (discount.getPercent() > d) {
								d = discount.getPercent();
							}
						}
					}
				} else {
					if (item.equals(discount.getItem())) {
						if (discount.getPercent() > d) {
							d = discount.getPercent();
						}
					}
				}
			}
		}
		return d;
	}
}
