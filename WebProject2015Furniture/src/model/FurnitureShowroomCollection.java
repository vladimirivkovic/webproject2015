package model;

import java.util.ArrayList;
import java.util.HashMap;

public class FurnitureShowroomCollection {
	private HashMap<String, FurnitureShowroom> showrooms;
	
	public FurnitureShowroomCollection() {
		showrooms = new HashMap<String, FurnitureShowroom>();
	}

	public ArrayList<FurnitureShowroom> getShowrooms() {
		return new ArrayList<FurnitureShowroom>(showrooms.values());
	}
	
	public FurnitureShowroom getShowroom(String id) {
		return showrooms.get(id);
	}

	public void addShowroom(FurnitureShowroom furnitureShowroom) {
		showrooms.put(furnitureShowroom.getPib(), furnitureShowroom);
		
	}
}
