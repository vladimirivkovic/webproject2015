package model;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class Bill {
	private static int seed = 0;
	
	private int id;
	private User customer;
	private Date dateAndTime;
	private double totalPrice;
	private double tax;
	private HashMap<String, ShoppingCartItem> items;
	private HashMap<String, Double> prices;

	public Bill(User user, FurnitureTypeCollection categories, DiscountCollection discounts, AdditionalServiceCollection services) {
		this.id = getNextId();
		this.customer = user;
		this.dateAndTime = new Date();
		items = new HashMap<String, ShoppingCartItem>();
		prices = new HashMap<String, Double>();
		totalPrice = 0;
		for (ShoppingCartItem item : user.getCart().getItems()) {
			double x = 0;
			items.put(item.getProduct().getCode(), item);
			item.getProduct().setCount(item.getProduct().getCount() - item.getCount());
			x = item.getProduct().getPrice() * item.getCount() 
					* (1-discounts.getMaxDiscountFor(item.getProduct(), categories)*0.01);
			totalPrice += x;
			
			for (AdditionalService service : item.getServices()) {
				if(services.getService(service.getName()) != null) {
					totalPrice += service.getPrice();
					x += service.getPrice();
				}
			}
			
			prices.put(item.getProduct().getCode(), x);
		}
	}

	public void setPrices(HashMap<String, Double> prices) {
		this.prices = prices;
	}

	public Bill() {
		items = new HashMap<String, ShoppingCartItem>();
		prices = new HashMap<String, Double>();
	}

	public User getCustomer() {
		return customer;
	}
	
	public void setCustomer(User u) {
		customer = u;
	}

	public Date getDateAndTime() {
		return dateAndTime;
	}

	public void setDateAndTime(Date dateAndTime) {
		this.dateAndTime = dateAndTime;
	}

	public double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public double getTax() {
		return tax;
	}

	public void setTax(double tax) {
		this.tax = tax;
	}

	public ArrayList<ShoppingCartItem> getItems() {
		return new ArrayList<ShoppingCartItem>(items.values());
	}
	
	public HashMap<String, Double> getPrices() {
		return prices;
	}

	public void setItems(HashMap<String, ShoppingCartItem> items) {
		this.items = items;
	}
	
	public static int getNextId() {
		return ++seed;
	}
	
	public static void setSeed(int s) {
		seed = s;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj == null)
			return false;
		if(obj instanceof Bill) {
			return ((Bill) obj).getId() == this.getId();
		} else {
			return false;
		}
	}

	public static boolean canBuy(User user) {
		boolean result = true;
		ArrayList<ShoppingCartItem> cartItems = user.getCart().getItems();
		
		for (ShoppingCartItem cartItem : cartItems) {
			if(cartItem.getProduct().getCount() < cartItem.getCount()) {
				result = false;
				break;
			}
		}
		
		return result;
	}
}
