package model;

import java.util.ArrayList;
import java.util.HashMap;

public class BillCollection {
	private HashMap<Integer, Bill> accounts;
	
	public BillCollection() {
		accounts = new HashMap<Integer, Bill>();
	}
	
	public ArrayList<Bill> getBills() {
		return new ArrayList<Bill>(accounts.values());
	}
	
	public boolean addAccount(Bill a) {
		if(accounts.containsKey(a.getId())) {
			return false;
		} else {
			accounts.put(a.getId(), a);
			return true;
		}
	}
}
