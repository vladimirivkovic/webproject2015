package model;

public class FurnitureItem {
	private String code;
	private String name;
	private int[] color;
	private String countryOfOrigin;
	private String producer;
	private double price;
	private int count;
	private FurnitureType type;
	private int year;
	private FurnitureShowroom showroom;
	private String imageOrVideo;
	private boolean deleted;
	
	public FurnitureItem() {
		this.deleted = false;
	}
	
	

	public FurnitureItem(String code, String name,
			String procuder, double price, FurnitureType type, FurnitureShowroom showroom, int count) {
		super();
		this.code = code;
		this.name = name;
		this.producer = procuder;
		this.price = price;
		this.type = type;
		this.showroom = showroom;
		this.count = count;
		
		this.deleted = false;
	}
	
	



	public String getCode() {
		return code;
	}

	public FurnitureItem(String code, String name, int[] color,
			String countryOfOrigin, String producer, double price, int count,
			FurnitureType type, int year, FurnitureShowroom showroom,
			String imageOrVideo, boolean deleted) {
		super();
		this.code = code;
		this.name = name;
		this.color = color;
		this.countryOfOrigin = countryOfOrigin;
		this.producer = producer;
		this.price = price;
		this.count = count;
		this.type = type;
		this.year = year;
		this.showroom = showroom;
		this.imageOrVideo = imageOrVideo;
		this.deleted = deleted;
	}



	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int[] getColor() {
		return color;
	}

	public void setColor(int[] color) {
		this.color = color;
	}

	public String getCountryOfOrigin() {
		return countryOfOrigin;
	}

	public void setCountryOfOrigin(String countryOfOrigin) {
		this.countryOfOrigin = countryOfOrigin;
	}

	public String getProducer() {
		return producer;
	}

	public void setProducer(String procuder) {
		this.producer = procuder;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public FurnitureType getType() {
		return type;
	}

	public void setType(FurnitureType type) {
		this.type = type;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public FurnitureShowroom getShowroom() {
		return showroom;
	}

	public void setShowroom(FurnitureShowroom showroom) {
		this.showroom = showroom;
	}

	public String getImageOrVideo() {
		return imageOrVideo;
	}

	public void setImageOrVideo(String imageOrVideo) {
		this.imageOrVideo = imageOrVideo;
	}
	
	public void setDeleted() {
		deleted = true;
	}
	
	public boolean isDeleted() {
		return deleted;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj == null)
			return false;
		if(obj instanceof FurnitureItem) {
			return ((FurnitureItem) obj).getCode().equals(this.getCode());
		} else {
			return false;
		}
	}
	
}
