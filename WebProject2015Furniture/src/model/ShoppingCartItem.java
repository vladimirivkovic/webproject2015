package model;

import java.util.ArrayList;
import java.util.HashMap;

public class ShoppingCartItem {
	private FurnitureItem product;
	private HashMap<String, AdditionalService> services;
	private int count;
	
	public ShoppingCartItem() {
		services = new HashMap<String, AdditionalService>();
	}

	public ShoppingCartItem(FurnitureItem product, int count) {
		super();
		this.product = product;
		this.count = count;
		services = new HashMap<String, AdditionalService>();
	}

	public FurnitureItem getProduct() {
		return product;
	}

	public void setProduct(FurnitureItem product) {
		this.product = product;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}
	
	public ArrayList<AdditionalService> getServices() {
		return new ArrayList<AdditionalService>(services.values());
	}
	
	public HashMap<String, AdditionalService> getServicesMap() {
		return services;
	}
	
	public void setServices(HashMap<String, AdditionalService> services) {
		this.services = services;
	}
	
	public void addService(AdditionalService service) {
		services.put(service.getName(), service);
	}
}
