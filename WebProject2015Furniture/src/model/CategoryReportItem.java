package model;

import java.util.HashMap;

public class CategoryReportItem {
	private HashMap<String, Integer> soldItems;
	
	public CategoryReportItem() {
		soldItems = new HashMap<String, Integer>();
	}


	public HashMap<String, Integer> getSoldItems() {
		return soldItems;
	}

	public void setSoldItems(HashMap<String, Integer> soldItems) {
		this.soldItems = soldItems;
	}
	
	public void addItem(String code, int count) {
		if(soldItems.containsKey(code)) {
			soldItems.put(code, soldItems.get(code)+count);
		} else {
			soldItems.put(code, count);
		}
	}
}
