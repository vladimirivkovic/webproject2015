package model;

public class User {
	private String username;
	private String password;
	private String name;
	private String surname;
	private Role role;
	private String telephone;
	private String email;
	private ShoppingCart cart;

	public User()
	{
		cart = new ShoppingCart();
	}

	public User(String username, String password, Role role) {
		super();
		this.username = username;
		this.password = password;
		this.role = role;
		this.cart = new ShoppingCart();
	}



	public User(String username, String password, String name, String surname,
			Role role, String telephone, String email) {
		super();
		this.username = username;
		this.password = password;
		this.name = name;
		this.surname = surname;
		this.role = role;
		this.telephone = telephone;
		this.email = email;
		this.cart = new ShoppingCart();
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public boolean isAdmin() {
		return role == Role.ADMIN;
	}
	
	public boolean isManager() {
		return role == Role.MANAGER;
	}
	
	public boolean isCustomer() {
		return role == Role.CUSTOMER;
	}

	public enum Role {
		ADMIN, MANAGER, CUSTOMER
	}
	
	public ShoppingCart getCart() {
		return cart;
	}

	public void setCart(ShoppingCart cart) {
		this.cart = cart;
	}
	
	public void emptyCart() {
		cart.empty();
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj == null)
			return false;
		if(obj instanceof User) {
			return ((User) obj).getUsername().equals(this.getUsername());
		} else {
			return false;
		}
	}
}
