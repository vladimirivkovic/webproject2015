package model;

import java.util.ArrayList;
import java.util.Date;

public class DayReportItem {
	private FurnitureShowroom showroom;
	private Date start;
	private Date stop;
	private ArrayList<Double> amounts;
	
	public DayReportItem() {
		amounts = new ArrayList<Double>();
	}
	
	public DayReportItem(Date start, Date stop, FurnitureShowroom showroom) {
		amounts = new ArrayList<Double>();
		Date d = (Date) start.clone();
		
		while(!d.after(stop)) {
			amounts.add(0.0);
			d.setTime(d.getTime() +  24*60*60*1000);
		}
		
		this.start = start;
		this.stop = stop;
		this.showroom = showroom;
	}
	
	public void addAmountAt(int i, double a) {
		amounts.set(i, a + amounts.get(i));
	}

	public FurnitureShowroom getShowroom() {
		return showroom;
	}

	public void setShowroom(FurnitureShowroom showroom) {
		this.showroom = showroom;
	}

	public Date getStart() {
		return start;
	}

	public void setStart(Date start) {
		this.start = start;
	}

	public Date getStop() {
		return stop;
	}

	public void setStop(Date stop) {
		this.stop = stop;
	}

	public ArrayList<Double> getAmounts() {
		return amounts;
	}

	public void setAmounts(ArrayList<Double> amounts) {
		this.amounts = amounts;
	}
	
	
}
