package serialization;

import java.util.ArrayList;

import model.AdditionalServiceCollection;
import model.FurnitureItemCollection;
import model.ShoppingCart;
import model.ShoppingCartItem;
import model.User;

public class ShoppingCartWrapper {
	private String username;
	private ArrayList<ShoppingCartItemWrapper> items;

	public ShoppingCartWrapper() {
		items = new ArrayList<ShoppingCartItemWrapper>();
	}

	public ShoppingCartWrapper(User user) {
		this.username = user.getUsername();
		items = new ArrayList<ShoppingCartItemWrapper>();
		for (ShoppingCartItem cartItem : user.getCart().getItems()) {
			items.add(new ShoppingCartItemWrapper(cartItem));
		}
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public ArrayList<ShoppingCartItemWrapper> getItems() {
		return items;
	}

	public void setItems(ArrayList<ShoppingCartItemWrapper> items) {
		this.items = items;
	}

	public ShoppingCart getCart(FurnitureItemCollection items,
			AdditionalServiceCollection services) {
		ShoppingCart cart = new ShoppingCart();
		if (this.items != null) {
			for (ShoppingCartItemWrapper sciw : this.items) {
				cart.addToShoppingCart(sciw.getCartItem(items, services));
			}
		}

		return cart;
	}

}
