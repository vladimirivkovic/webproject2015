package serialization;

import java.util.ArrayList;

import model.AdditionalServiceCollection;
import model.FurnitureItemCollection;
import model.User;
import model.UsersCollection;

public class ShoppingCartWrapperCollection {
	private ArrayList<ShoppingCartWrapper> carts;
	
	public ShoppingCartWrapperCollection() {
		// TODO Auto-generated constructor stub
	}
	
	public ShoppingCartWrapperCollection(UsersCollection users) {
		carts = new ArrayList<ShoppingCartWrapper>();
		for(User user : users.getUsers()) {
			carts.add(new ShoppingCartWrapper(user));
		}
	}

	public ArrayList<ShoppingCartWrapper> getCarts() {
		return carts;
	}

	public void setCarts(ArrayList<ShoppingCartWrapper> carts) {
		this.carts = carts;
	}
	
	public void setCartsToUsers(UsersCollection users, FurnitureItemCollection items,
			AdditionalServiceCollection services) {
		for(ShoppingCartWrapper scw : carts) {
			users.getUser(scw.getUsername()).setCart(scw.getCart(items, services));
		}
	}
}
