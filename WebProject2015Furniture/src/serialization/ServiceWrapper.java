package serialization;

import model.AdditionalService;

public class ServiceWrapper {
	private String name;
	private String description;
	private double price;
	private String item;
	
	public ServiceWrapper() {
		// TODO Auto-generated constructor stub
	}
	
	public ServiceWrapper(AdditionalService service) {
		this.name = service.getName();
		this.description = service.getDescription();
		this.price = service.getPrice();
		this.item = service.getItem().getCode();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getItem() {
		return item;
	}

	public void setItem(String item) {
		this.item = item;
	}
}
