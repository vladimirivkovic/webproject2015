package serialization;

import java.util.ArrayList;
import java.util.HashMap;

import model.Discount;
import model.DiscountCollection;
import model.FurnitureItemCollection;
import model.FurnitureShowroomCollection;
import model.FurnitureTypeCollection;

public class DiscountWrapperCollection {
	private ArrayList<DiscountWrapper> discounts;
	
	public DiscountWrapperCollection() {
		// TODO Auto-generated constructor stub
	}
	
	public DiscountWrapperCollection(DiscountCollection discounts) {
		this.discounts = new ArrayList<DiscountWrapper>();
		for(Discount discount : discounts.getDiscounts()) {
			this.discounts.add(new DiscountWrapper(discount));
		}
	}
	
	public ArrayList<DiscountWrapper> getDiscounts() {
		return discounts;
	}

	public void setDiscounts(ArrayList<DiscountWrapper> discounts) {
		this.discounts = discounts;
	}

	public DiscountCollection makeCollection(FurnitureItemCollection items,
			FurnitureTypeCollection types, FurnitureShowroomCollection showrooms) {
		DiscountCollection collection = new DiscountCollection();
		for(DiscountWrapper dw : discounts) {
			if(dw.getIt() == 0) {
				collection.addDiscount(new Discount(dw.getStart(), dw.getFinish(),
					dw.getPercent(), dw.getIt(), items.getItem(dw.getItem()), null, null));
			} else {
				collection.addDiscount(new Discount(dw.getStart(), dw.getFinish(),
						dw.getPercent(), dw.getIt(), null,
						types.getType(dw.getType()), showrooms.getShowroom(dw.getShowroom())));
			}
		}
		
		return collection;
	}
	
	
}
