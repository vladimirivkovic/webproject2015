package serialization;

import java.util.ArrayList;
import java.util.HashMap;

import model.AdditionalService;
import model.AdditionalServiceCollection;
import model.FurnitureItemCollection;
import model.ShoppingCartItem;

public class ShoppingCartItemWrapper {
	private String product;
	private ArrayList<String> services;
	private int count;
	
	public ShoppingCartItemWrapper() {
		// TODO Auto-generated constructor stub
	}
	
	public ShoppingCartItemWrapper(ShoppingCartItem sci) {
		this.product = sci.getProduct().getCode();
		this.count = sci.getCount();
		services = new ArrayList<String>();
		for (AdditionalService as : sci.getServices()) {
			services.add(as.getName());
		}
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public ArrayList<String> getServices() {
		return services;
	}

	public void setServices(ArrayList<String> services) {
		this.services = services;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}
	
	public ShoppingCartItem getCartItem(FurnitureItemCollection items,
			AdditionalServiceCollection services) {
		ShoppingCartItem item = new ShoppingCartItem(items.getItem(product), count);
		for(String s : this.services) {
			item.addService(services.getService(s));
		}
		
		
		return item;
	}
	
}
