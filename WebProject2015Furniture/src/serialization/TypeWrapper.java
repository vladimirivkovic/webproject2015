package serialization;

import model.FurnitureType;

public class TypeWrapper {
	private String name;
	private String description;
	private String superType;
	private boolean deleted;
	
	
	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	
	
	public TypeWrapper() {
		// TODO Auto-generated constructor stub
	}
	
	public TypeWrapper(FurnitureType type) {
		this.name = type.getName();
		this.description = type.getDescription();
		if(type.getSuperType() != null) {
			this.superType = type.getSuperType().getName();
		} else {
			this.superType = null;
		}
		this.deleted = type.isDeleted();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSuperType() {
		return superType;
	}

	public void setSuperType(String superType) {
		this.superType = superType;
	}
	
	
}
