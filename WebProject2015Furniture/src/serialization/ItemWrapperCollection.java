package serialization;

import java.util.ArrayList;
import java.util.HashMap;

import model.FurnitureItem;
import model.FurnitureItemCollection;
import model.FurnitureShowroomCollection;
import model.FurnitureTypeCollection;

public class ItemWrapperCollection {
	private ArrayList<ItemWrapper> items;
	
	public ItemWrapperCollection() {
		// TODO Auto-generated constructor stub
	}
	
	public ItemWrapperCollection(FurnitureItemCollection items) {
		this.items = new ArrayList<ItemWrapper>();
		for(FurnitureItem item :items.getItems()) {
			this.items.add(new ItemWrapper(item));
		}
	}

	public ArrayList<ItemWrapper> getItems() {
		return items;
	}

	public void setItems(ArrayList<ItemWrapper> items) {
		this.items = items;
	}
	
	public FurnitureItemCollection makeCollection(
			FurnitureTypeCollection types, FurnitureShowroomCollection showrooms) {
		FurnitureItemCollection collection = new FurnitureItemCollection();
		for(ItemWrapper iw : items) {
			collection.addItem(new FurnitureItem(iw.getCode(), iw.getName(),
					iw.getColor(), iw.getCountryOfOrigin(), iw.getProducer(),
					iw.getPrice(), iw.getCount(), types.getType(iw.getType()),
					iw.getYear(), showrooms.getShowroom(iw.getShowroom()),
					iw.getImageOrVideo(), iw.isDeleted()));
		}
		
		return collection;
	}
}
