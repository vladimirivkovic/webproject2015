package serialization;

import java.util.ArrayList;
import java.util.HashMap;

import model.AdditionalService;
import model.AdditionalServiceCollection;
import model.FurnitureItemCollection;

public class ServicesWrapperCollection {
	private ArrayList<ServiceWrapper> services;
	
	public ServicesWrapperCollection() {
		// TODO Auto-generated constructor stub
	}
	
	public ServicesWrapperCollection(AdditionalServiceCollection services) {
		this.services = new ArrayList<ServiceWrapper>();
		for(AdditionalService service : services.getServices()) {
			this.services.add(new ServiceWrapper(service));
		}
	}

	public ArrayList<ServiceWrapper> getServices() {
		return services;
	}

	public void setServices(ArrayList<ServiceWrapper> services) {
		this.services = services;
	}
	
	public AdditionalServiceCollection makeCollection(FurnitureItemCollection items) {
		AdditionalServiceCollection collection = new AdditionalServiceCollection();
		for(ServiceWrapper sw : services) {
			collection.addService(new AdditionalService(sw.getName(), sw.getDescription(),
					sw.getPrice(), items.getItem(sw.getItem())));
		}
		
		return collection;
	}
}
