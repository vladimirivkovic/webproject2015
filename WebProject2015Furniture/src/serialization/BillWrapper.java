package serialization;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import model.AdditionalServiceCollection;
import model.Bill;
import model.FurnitureItemCollection;
import model.ShoppingCartItem;
import model.UsersCollection;

public class BillWrapper {
	private int id;
	private String customer;
	private Date dateAndTime;
	private double totalPrice;
	private double tax;
	private ArrayList<ShoppingCartItemWrapper> items;
	private ArrayList<Double> prices;
	
	public BillWrapper() {
		
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCustomer() {
		return customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}

	public Date getDateAndTime() {
		return dateAndTime;
	}

	public void setDateAndTime(Date dateAndTime) {
		this.dateAndTime = dateAndTime;
	}

	public double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public double getTax() {
		return tax;
	}

	public void setTax(double tax) {
		this.tax = tax;
	}

	public ArrayList<ShoppingCartItemWrapper> getItems() {
		return items;
	}

	public void setItems(ArrayList<ShoppingCartItemWrapper> items) {
		this.items = items;
	}

	public ArrayList<Double> getPrices() {
		return prices;
	}

	public void setPrices(ArrayList<Double> prices) {
		this.prices = prices;
	}

	public BillWrapper(Bill b) {
		this.id = b.getId();
		this.customer = b.getCustomer().getUsername();
		this.dateAndTime = b.getDateAndTime();
		this.totalPrice = b.getTotalPrice();
		this.tax = b.getTax();
		this.prices = new ArrayList<Double>(b.getPrices().values());
		this.items = new ArrayList<ShoppingCartItemWrapper>();
		for (ShoppingCartItem sci : b.getItems()) {
			this.items.add(new ShoppingCartItemWrapper(sci));
		}
	}
	
	public Bill makeBill(UsersCollection users,
			FurnitureItemCollection items, AdditionalServiceCollection services) {
		Bill bill = new Bill();
		bill.setCustomer(users.getUser(customer));
		bill.setDateAndTime(dateAndTime);
		bill.setId(id);
		bill.setTax(tax);
		bill.setTotalPrice(totalPrice);
		HashMap<String, ShoppingCartItem> billItems = new HashMap<String, ShoppingCartItem>();
		HashMap<String, Double> billPrices = new HashMap<String, Double>();
		int i=0;
		for(ShoppingCartItemWrapper sciw : this.items) {
			ShoppingCartItem sci = sciw.getCartItem(items, services);
			billItems.put(sci.getProduct().getCode(), sci);
			billPrices.put(sci.getProduct().getCode(), prices.get(i));
			i++;
		}
		bill.setItems(billItems);
		bill.setPrices(billPrices);
		
		
		return bill;
	}
}
