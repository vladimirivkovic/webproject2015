package serialization;

import java.util.Date;

import model.Discount;

public class DiscountWrapper {
	private int id;
	private Date start;
	private Date finish;
	private double percent;
	private int it;
	private String item;
	private String type;
	private String showroom;
	
	public DiscountWrapper() {
		// TODO Auto-generated constructor stub
	}
	
	public DiscountWrapper(Discount discount) {
		this.id = discount.getId();
		this.start = discount.getStart();
		this.finish = discount.getFinish();
		this.percent = discount.getPercent();
		this.it = discount.getIt();
		if(it == 0) {
			this.item = discount.getItem().getCode();
			this.type = null;
		} else {
			this.item = null;
			this.type = discount.getType().getName();
		}
		this.showroom = discount.getShowroom().getPib();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getStart() {
		return start;
	}

	public void setStart(Date start) {
		this.start = start;
	}

	public Date getFinish() {
		return finish;
	}

	public void setFinish(Date finish) {
		this.finish = finish;
	}

	public double getPercent() {
		return percent;
	}

	public void setPercent(double percent) {
		this.percent = percent;
	}

	public int getIt() {
		return it;
	}

	public void setIt(int it) {
		this.it = it;
	}

	public String getItem() {
		return item;
	}

	public void setItem(String item) {
		this.item = item;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getShowroom() {
		return showroom;
	}

	public void setShowroom(String showroom) {
		this.showroom = showroom;
	}
}
