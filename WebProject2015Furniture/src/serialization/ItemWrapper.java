package serialization;

import model.FurnitureItem;


public class ItemWrapper {
	private String code;
	private String name;
	private int[] color;
	private String countryOfOrigin;
	private String producer;
	private double price;
	private int count;
	private String type;
	private int year;
	private String showroom;
	private String imageOrVideo;
	
	private boolean deleted;
	
	public ItemWrapper() {
		// TODO Auto-generated constructor stub
	}
	
	public ItemWrapper(FurnitureItem item) {
		this.code = item.getCode();
		this.name = item.getName();
		this.color = item.getColor();
		this.countryOfOrigin = item.getCountryOfOrigin();
		this.producer = item.getProducer();
		this.price = item.getPrice();
		this.count = item.getCount();
		this.type = item.getType().getName();
		this.year = item.getYear();
		this.showroom = item.getShowroom().getPib();
		this.imageOrVideo = item.getImageOrVideo();
		this.deleted = item.isDeleted();
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int[] getColor() {
		return color;
	}

	public void setColor(int[] color) {
		this.color = color;
	}

	public String getCountryOfOrigin() {
		return countryOfOrigin;
	}

	public void setCountryOfOrigin(String countryOfOrigin) {
		this.countryOfOrigin = countryOfOrigin;
	}

	public String getProducer() {
		return producer;
	}

	public void setProducer(String producer) {
		this.producer = producer;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public String getShowroom() {
		return showroom;
	}

	public void setShowroom(String showroom) {
		this.showroom = showroom;
	}

	public String getImageOrVideo() {
		return imageOrVideo;
	}

	public void setImageOrVideo(String imageOrVideo) {
		this.imageOrVideo = imageOrVideo;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
}
