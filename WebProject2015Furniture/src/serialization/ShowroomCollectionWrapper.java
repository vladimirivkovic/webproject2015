package serialization;

import java.util.ArrayList;

import model.FurnitureShowroom;
import model.FurnitureShowroomCollection;

public class ShowroomCollectionWrapper {
	ArrayList<FurnitureShowroom> showrooms;
	
	public ShowroomCollectionWrapper() {
		// TODO Auto-generated constructor stub
	}
	
	public ShowroomCollectionWrapper(FurnitureShowroomCollection fsc) {
		showrooms = new ArrayList<FurnitureShowroom>();
		for(FurnitureShowroom fs : fsc.getShowrooms()) {
			showrooms.add(fs);
		}
	}
	
	public FurnitureShowroomCollection getCollection() {
		FurnitureShowroomCollection collection = new FurnitureShowroomCollection();
		for(FurnitureShowroom fs : showrooms) {
			collection.addShowroom(fs);
		}
		return collection;
	}

	public ArrayList<FurnitureShowroom> getShowrooms() {
		return showrooms;
	}

	public void setShowrooms(ArrayList<FurnitureShowroom> showrooms) {
		this.showrooms = showrooms;
	}
}
