package serialization;

import java.util.ArrayList;

import model.AdditionalServiceCollection;
import model.Bill;
import model.BillCollection;
import model.FurnitureItemCollection;
import model.UsersCollection;

public class BillWrapperCollection {
	private ArrayList<BillWrapper> bills;
	
	public BillWrapperCollection() {
		bills = new ArrayList<BillWrapper>();
	}
	
	public BillWrapperCollection(BillCollection collection) {
		bills = new ArrayList<BillWrapper>();
		for(Bill bill : collection.getBills()) {
			bills.add(new BillWrapper(bill));
		}
	}

	public ArrayList<BillWrapper> getBills() {
		return bills;
	}

	public void setBills(ArrayList<BillWrapper> bills) {
		this.bills = bills;
	}
	
	public BillCollection makeCollection(UsersCollection users,
			FurnitureItemCollection items, AdditionalServiceCollection services) {
		BillCollection collection = new BillCollection();
		for(BillWrapper bw : bills) {
			collection.addAccount(bw.makeBill(users, items, services));
		}
		
		return collection;
	}
}
