package serialization;

import java.util.ArrayList;

import model.FurnitureType;
import model.FurnitureTypeCollection;

public class TypeWrapperCollection {
	private ArrayList<TypeWrapper> types;
	
	public TypeWrapperCollection() {
		// TODO Auto-generated constructor stub
	}
	
	public TypeWrapperCollection(FurnitureTypeCollection types) {
		this.types = new ArrayList<TypeWrapper>();
		for(FurnitureType type : types.getTypes()) {
			this.types.add(new TypeWrapper(type));
		}
	}

	public ArrayList<TypeWrapper> getTypes() {
		return types;
	}

	public void setTypes(ArrayList<TypeWrapper> types) {
		this.types = types;
	}
	
	public FurnitureTypeCollection makeCollection() {
		FurnitureTypeCollection collection = new FurnitureTypeCollection();
		ArrayList<TypeWrapper> deleted = new ArrayList<TypeWrapper>();
		
		for(TypeWrapper type : types) {
			if(type.getSuperType() == null) {
				FurnitureType ft = new FurnitureType(type.getName(), type.getDescription(), null);
				if(type.isDeleted()) {
					ft.setDeleted();
				}
				collection.addType(ft);
				deleted.add(type);
			}
		}
		
		for(TypeWrapper s : deleted) {
			types.remove(s);
		}
		deleted.clear();
		
		while(types.size() > 0) {
			for(TypeWrapper type : types) {
				if(collection.getType(type.getSuperType()) != null) {
					FurnitureType ft = new FurnitureType(type.getName(),
										type.getDescription(),
										collection.getType(type.getSuperType()));
					
					if(type.isDeleted()) {
						ft.setDeleted();
					}
					collection.addType(ft);
					deleted.add(type);
				}
			}
			for(TypeWrapper s : deleted) {
				types.remove(s);
			}
			deleted.clear();
		}
		
		return collection;
	}
}
