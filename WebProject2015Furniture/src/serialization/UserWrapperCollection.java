package serialization;

import java.util.ArrayList;
import java.util.HashMap;

import model.User;
import model.UsersCollection;

public class UserWrapperCollection {
	private ArrayList<UserWrapper> users;
	
	public UserWrapperCollection() {
		// TODO Auto-generated constructor stub
	}
	
	public UserWrapperCollection(UsersCollection users) {
		this.users = new ArrayList<UserWrapper>();
		for(User u : users.getUsers()) {
			this.users.add(new UserWrapper(u));
		}
	}

	public ArrayList<UserWrapper> getUsers() {
		return users;
	}

	public void setUsers(ArrayList<UserWrapper> users) {
		this.users = users;
	}
	
	public UsersCollection getCollection() {
		UsersCollection collection = new UsersCollection();
		for(UserWrapper uw : users) {
			collection.addUser(new User(uw.getUsername(), uw.getPassword(),
					uw.getName(), uw.getSurname(), uw.getRole(),
					uw.getTelephone(), uw.getEmail()));
		}
		
		
		return collection;
	}
}
