package servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;

import model.User;
import model.User.Role;
import model.UsersCollection;

/**
 * Servlet implementation class LoginSevlet
 */
public class LoginSevlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginSevlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    @Override
    public void init() throws ServletException {
    	super.init();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {	
		User currentUser = (User) request.getSession().getAttribute("user");
		if(currentUser == null) {
			response.sendRedirect("login.html");
		} else {
			response.sendRedirect("index.html");
		}
	}
		

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		
		Role result = null;
		
		if(username == null) {
			User currentUser = (User) request.getSession().getAttribute("user");
			if(currentUser != null) {
				result = currentUser.getRole();
			}
		} else {
			UsersCollection users = (UsersCollection) getServletContext().getAttribute("users");
			User u = users.validate(username, password);
			if(u != null) {
				request.getSession().setAttribute("user", u);
				result = u.getRole();
			} else {
				result = null;
			}
		}
		
		response.setContentType("application/json");
		
		ObjectMapper mapper = new ObjectMapper();
		mapper.writeValue(response.getOutputStream(), result);
	}

}
