package servlets;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.FurnitureItem;
import model.FurnitureItemCollection;
import model.FurnitureShowroom;
import model.FurnitureShowroomCollection;
import model.FurnitureType;
import model.FurnitureTypeCollection;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 * Servlet implementation class UploadServlet
 */
public class UploadServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	
	private static final String UPLOAD_DIRECTORY = "upload";
	private static final int THRESHOLD_SIZE     = 1024 * 1024 * 3;  // 3MB
	private static final int MAX_FILE_SIZE      = 1024 * 1024 * 40; // 40MB
	private static final int MAX_REQUEST_SIZE   = 1024 * 1024 * 50; // 50MB
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UploadServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// checks if the request actually contains upload file
        if (!ServletFileUpload.isMultipartContent(request)) {
            PrintWriter writer = response.getWriter();
            writer.println("Request does not contain upload data");
            writer.flush();
            return;
        }
        FurnitureTypeCollection ftc = (FurnitureTypeCollection) getServletContext()
				.getAttribute("categories");
		FurnitureShowroomCollection fsc = (FurnitureShowroomCollection) getServletContext()
				.getAttribute("showrooms");
		FurnitureItemCollection items = (FurnitureItemCollection) getServletContext()
				.getAttribute("items");
		String mod = "";
        
        
        HashMap<String, String> params = new HashMap<String, String>();
        String fileName = "";
        
         
        // configures upload settings
        DiskFileItemFactory factory = new DiskFileItemFactory();
        factory.setSizeThreshold(THRESHOLD_SIZE);
        factory.setRepository(new File(System.getProperty("java.io.tmpdir")));
         
        ServletFileUpload upload = new ServletFileUpload(factory);
        upload.setSizeMax(MAX_FILE_SIZE);
        upload.setSizeMax(MAX_REQUEST_SIZE);
         
        // constructs the directory path to store upload file
        String uploadPath = getServletContext().getRealPath("")
            + File.separator + UPLOAD_DIRECTORY;
        // creates the directory if it does not exist
        File uploadDir = new File(uploadPath);
        if (!uploadDir.exists()) {
            uploadDir.mkdir();
        }
         
        try {
            // parses the request's content to extract file data
            List formItems = upload.parseRequest(request);
            Iterator iter = formItems.iterator();
             
            // iterates over form's fields
            while (iter.hasNext()) {
                FileItem item = (FileItem) iter.next();
                // processes only fields that are not form fields
                if (!item.isFormField()) {
                    fileName = new File(item.getName()).getName();
                    String filePath = uploadPath + File.separator + fileName;
                    File storeFile = new File(filePath);
                    
                    System.out.println(fileName);
                     
                    // saves the file on disk
                    item.write(storeFile);
                } else {
                	String name = item.getFieldName();
                	String value = item.getString("UTF-8");
                	params.put(name, value);
                	System.out.println(name + " : " + value);
                }
            }
            FurnitureItem fi = new FurnitureItem();
            
            if(params.containsKey("code")) {
            	fi.setCode(params.get("code"));
            } else {
            	response.sendError(204);
            	return;
            }
            if(params.containsKey("name")) {
            	fi.setName(params.get("name"));
            } else {
            	response.sendError(204);
            	return;
            }
            if(params.containsKey("type")) {
            	FurnitureType ft = ftc.getActiveType(params.get("type"));
            	if(ft != null)
            		fi.setType(ft);
            	else {
            		response.sendError(204);
            		return;
            	}
            } else {
            	response.sendError(204);
            	return;
            }
            if(params.containsKey("showroom")) {
            	FurnitureShowroom fs = fsc.getShowroom(params.get("showroom"));
            	if(fs != null) 
            		fi.setShowroom(fs);
            	else {
            		response.sendError(204);
            		return;
            	}
            } else {
            	response.sendError(204);
            	return;
            }
            if(params.containsKey("count")) {
            	fi.setCount(Integer.parseInt(params.get("count")));
            } else {
            	response.sendError(204);
            	return;
            }
            if(params.containsKey("country")) {
            	fi.setCountryOfOrigin(params.get("country"));
            } else {
            	response.sendError(204);
            	return;
            }
            if(params.containsKey("manufac")) {
            	fi.setProducer(params.get("manufac"));
            } else {
            	response.sendError(204);
            	return;
            }
            if(params.containsKey("year")) {
            	fi.setYear(Integer.parseInt(params.get("year")));
            } else {
            	response.sendError(204);
            	return;
            }
            if(params.containsKey("price")) {
            	fi.setPrice(Double.parseDouble(params.get("price")));
            } else {
            	response.sendError(204);
            	return;
            }
            if(params.containsKey("color")) {
            	String c = params.get("color");
            	int r,g,b;
            	r = Integer.parseInt(c.substring(1, 3), 16);
            	g = Integer.parseInt(c.substring(3, 5), 16);
            	b = Integer.parseInt(c.substring(5), 16);
            	int[] clr = new int[3];
            	clr[0] = r; clr[1] = g; clr[2] = b;
            	fi.setColor(clr);
            } else {
            	response.sendError(204);
            	return;
            }
            if(params.containsKey("mod")) {
            	mod = params.get("mod");
            } else {
            	response.sendError(204);
            	return;
            }
            if(!"".equals(fileName)) {
            	fi.setImageOrVideo(fileName);
            }
            
            switch (mod) {
			case "+":
				if(!items.addNewItem(fi)) {
	            	response.sendError(204);
	            	return;
	            }
				break;
			case "e":
				if(!items.setItem(fi)) {
	            	response.sendError(204);
	            	return;
	            }
				System.out.println(fi.getName() + " edited");
				break;

			default:
				response.sendError(204);
            	break;
			}
            
            
        } catch (Exception ex) {
        	response.sendError(204);
        }
        
	}

}
