package servlets;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;

import model.Bill;
import model.BillCollection;
import model.DayReportItem;
import model.FurnitureShowroom;
import model.FurnitureShowroomCollection;
import model.ShoppingCartItem;
import model.User;

/**
 * Servlet implementation class DayReportServlet
 */
public class DayReportServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DayReportServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		User currentUser = (User) request.getSession().getAttribute("user");
		if(currentUser == null || currentUser.getRole() != User.Role.MANAGER) {
			response.sendError(401);
			return;
		}
		
		String start = request.getParameter("start");
		String stop = request.getParameter("stop");
		
		if(start != null && stop != null) {
			try {
				HashMap<String, DayReportItem> reports = new HashMap<String, DayReportItem>();
				BillCollection ac = (BillCollection) getServletContext().getAttribute("bills");
				FurnitureShowroomCollection fsc = (FurnitureShowroomCollection) getServletContext()
						.getAttribute("showrooms");
				
				Date first = sdf.parse(start);
				Date last = sdf.parse(stop);
				
				for (FurnitureShowroom showroom : fsc.getShowrooms()) {
					reports.put(showroom.getPib(), new DayReportItem(first, last, showroom));
				}
				
				for (Bill a : ac.getBills()) {
					Date d = a.getDateAndTime();
					if(first.before(d) && last.after(d)) {
						long i = (d.getTime() - first.getTime())/(24*60*60*1000);
						for (ShoppingCartItem item : a.getItems()) {
							FurnitureShowroom sr = item.getProduct().getShowroom();
							//double price = item.getCount() * item.getProduct().getPrice();
							Double price = a.getPrices().get(item.getProduct().getCode());
							double p = (price == null) ? 0 : price.doubleValue();
							reports.get(sr.getPib()).addAmountAt((int) i, p);
						}
					}
				}
				
				response.setContentType("application/json");
				ObjectMapper mapper = new ObjectMapper();
				mapper.writeValue(response.getOutputStream(), reports.values());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}

}
