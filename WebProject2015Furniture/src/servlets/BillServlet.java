package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;

import model.AdditionalServiceCollection;
import model.Bill;
import model.BillCollection;
import model.DiscountCollection;
import model.FurnitureItemCollection;
import model.FurnitureTypeCollection;
import model.User;

/**
 * Servlet implementation class AccountServlet
 */
public class BillServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	@Override
	public void init() throws ServletException {
		super.init();
		
        if(getServletContext().getAttribute("bills") == null) {
        	BillCollection ac = new BillCollection();
        	
        	getServletContext().setAttribute("bills", ac);
        }
	}
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public BillServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		User currentUser = (User) request.getSession().getAttribute("user");
		if(currentUser == null || currentUser.getRole() != User.Role.MANAGER) {
			//response.sendRedirect("login.html");
			response.sendError(204);
			return;
		}
		
		
		BillCollection ac = (BillCollection) getServletContext().getAttribute("bills");
		
		response.setContentType("application/json");
		ObjectMapper mapper = new ObjectMapper();
		mapper.writeValue(response.getOutputStream(), ac.getBills());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		User user = (User) request.getSession().getAttribute("user");
		if(user == null || user.getRole() != User.Role.CUSTOMER) {
			response.sendError(401);
			return;
		}
		
		double amount = 0;
		
		response.setContentType("application/text");
		FurnitureTypeCollection categories = (FurnitureTypeCollection) getServletContext().getAttribute("categories");
		DiscountCollection discounts = (DiscountCollection) getServletContext().getAttribute("discounts");
		AdditionalServiceCollection services = (AdditionalServiceCollection) getServletContext().getAttribute("services");
		if(user != null) {
			BillCollection ac = (BillCollection) getServletContext().getAttribute("bills");
			System.out.println(user.getUsername() + " buys");
			//synchronized (categories) {
				if(Bill.canBuy(user)) {
					Bill a = new Bill(user, categories, discounts, services);
					ac.addAccount(a);
					user.emptyCart();
					amount = a.getTotalPrice();
				} else {
					response.sendError(204);
					return;
				}
				
			//}
			
			getServletContext().setAttribute("bills", ac);
		}
		
		
		ObjectMapper mapper = new ObjectMapper();
		mapper.writeValue(response.getOutputStream(), amount);
	}

}
