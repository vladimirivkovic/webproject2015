package servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;

import model.AdditionalServiceCollection;
import model.FurnitureItem;
import model.FurnitureItemCollection;
import model.FurnitureShowroom;
import model.FurnitureShowroomCollection;
import model.FurnitureType;
import model.FurnitureTypeCollection;
import model.User;

/**
 * Servlet implementation class ItemsServlet
 */
public class ItemsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ItemsServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		User currentUser = (User) request.getSession().getAttribute("user");
		if(currentUser == null) {
			response.sendError(204);
			//response.sendRedirect("login.html");
			return;
		}
		
		String code = request.getParameter("code");
		FurnitureItemCollection items = (FurnitureItemCollection) getServletContext()
				.getAttribute("items");

		response.setContentType("application/json");

		ObjectMapper mapper = new ObjectMapper();
		if(code == null) {
			mapper.writeValue(response.getOutputStream(), items.getActiveItems());
		} else {
			mapper.writeValue(response.getOutputStream(), items.getActiveItem(code));
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		User currentUser = (User) request.getSession().getAttribute("user");
		if(currentUser == null || currentUser.getRole() != User.Role.ADMIN) {
			response.sendError(401);
			return;
		}
		
		String code = request.getParameter("code");
		

		FurnitureItemCollection items = (FurnitureItemCollection) getServletContext()
				.getAttribute("items");

		/*FurnitureTypeCollection ftc = (FurnitureTypeCollection) getServletContext()
				.getAttribute("categories");
		FurnitureShowroomCollection fsc = (FurnitureShowroomCollection) getServletContext()
				.getAttribute("showrooms");*/
		
		if(code != null) {
				AdditionalServiceCollection services = (AdditionalServiceCollection) getServletContext().getAttribute("services");
				synchronized (services) {
					if(!items.deleteItem(code, services))
						{
							response.sendError(204);
							return;
						}
				}
		} else {
			response.sendError(204);
			return;
		}

/*		if (type != null && showroom != null) {
			for (FurnitureItem fi : items.getItems()) {
				if (type.equals("All categories")) {
					if(showroom.equals("All showrooms")) {
						selected.add(fi);
					}
					else if (fi.getShowroom().getPib().equals(showroom)) {
						selected.add(fi);
					}
				} else if (fi.getType().getName().equals(type)) {
					if(showroom.equals("All showrooms")) {
						selected.add(fi);
					}
					else if (fi.getShowroom().getPib().equals(showroom)) {
						selected.add(fi);
					}
				}
			}
		}*/
		
		getServletContext().setAttribute("items", items);

		/*response.setContentType("application/json");

		ObjectMapper mapper = new ObjectMapper();
		mapper.writeValue(response.getOutputStream(), selected);*/

	}

}
