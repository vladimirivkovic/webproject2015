package servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;

import model.FurnitureItemCollection;
import model.FurnitureType;
import model.FurnitureTypeCollection;
import model.User;

/**
 * Servlet implementation class CategoriesServlet
 */
public class CategoriesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	@Override
	public void init() throws ServletException {
		super.init();
	}
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CategoriesServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		User currentUser = (User) request.getSession().getAttribute("user");
		if(currentUser == null) {
			response.sendError(204);
			//response.sendRedirect("login.html");
			return;
		}
		
		String name = request.getParameter("name");
		
		response.setContentType("application/json");
		FurnitureTypeCollection categories = (FurnitureTypeCollection) getServletContext().getAttribute("categories");
		ArrayList<FurnitureType> result = categories.getActiveTypes();
		
		ObjectMapper mapper = new ObjectMapper();
		if(name == null || name.equals("")) {
			mapper.writeValue(response.getOutputStream(), result);
		} else {
			mapper.writeValue(response.getOutputStream(), categories.getActiveType(name));
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		User currentUser = (User) request.getSession().getAttribute("user");
		if(currentUser == null || currentUser.getRole() != User.Role.ADMIN) {
			response.sendError(401);
			return;
		}
		
		String name = request.getParameter("name");
		String desc = request.getParameter("desc");
		String spr = request.getParameter("spr");
		String mod = request.getParameter("mod");
		
		System.out.println(name + "," + desc + "," + spr);
		
		FurnitureTypeCollection categories = (FurnitureTypeCollection) getServletContext().getAttribute("categories");
		FurnitureType superType = categories.getActiveType(spr);
		FurnitureItemCollection fic = (FurnitureItemCollection) getServletContext()
				.getAttribute("items");
		
		
		/*if(desc != null) {
			if(categories.getActiveType(name) != null) {
				FurnitureType oldType = categories.getActiveType(name);
				if(mod != null && mod.equals("+")) {
					if(!categories.addNewType(new FurnitureType(name, desc, superType)))
						response.sendError(204);
				} else if(spr != null) {
					
					if(categories.areLinked(oldType, superType)) {
						response.sendError(204);
						System.out.println("loop in tree!!!");
					} else {
						oldType.setDescription(desc);
						oldType.setSuperType(superType);
						categories.setType(oldType);
					}
				} else {
					response.sendError(204);
				}
			} else if(spr != null) {
				categories.addNewType(new FurnitureType(name, desc, superType));
			} else {
				response.sendError(204);
			}
		} else {
			if(name != null) {
				FurnitureType deleted = null;
				if((deleted = categories.deleteType(name, fic)) != null) {
					@SuppressWarnings("unchecked")
					ArrayList<FurnitureType> archievedTypes = (ArrayList<FurnitureType>) getServletContext().getAttribute("archievedTypes");
					if(archievedTypes != null) {
						archievedTypes.add(deleted);
					}
				} else {
					response.sendError(204);
				}
			}
		}*/
		if(mod == null) {
			response.sendError(204);
			return;
		}
		
		switch (mod) {
		case "+":
			if(!"-".equals(spr) && superType == null) {
				response.sendError(204);
				return;
			}
			if(!categories.addNewType(new FurnitureType(name, desc, superType))) {
				response.sendError(204);
			}
			break;
		case "e":
			if(!"-".equals(spr) && superType == null) {
				response.sendError(204);
				return;
			}
			if(!categories.setType(new FurnitureType(name, desc, superType))) {
				response.sendError(204);
			}
			break;
		case "-":
			if(categories.deleteType(name, fic) == null) {
				response.sendError(204);
			}
			break;

		default:
			response.sendError(204);
			break;
		}
		
		getServletContext().setAttribute("categories", categories);	
		
		response.setContentType("application/text");
	}

}
