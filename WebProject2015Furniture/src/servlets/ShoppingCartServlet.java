package servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;

import model.AdditionalService;
import model.AdditionalServiceCollection;
import model.FurnitureItem;
import model.FurnitureItemCollection;
import model.ShoppingCartItem;
import model.User;

/**
 * Servlet implementation class ShoppingCartServlet
 */
public class ShoppingCartServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ShoppingCartServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		User user = (User) request.getSession().getAttribute("user");
		String code = request.getParameter("code");
		
		if(user != null) {
			response.setContentType("application/json");
			ObjectMapper mapper = new ObjectMapper();
			
			if(code != null) {
				ArrayList<AdditionalService> result = null;
				result = user.getCart().getServicesFor(code);
				if(result != null) {
					mapper.writeValue(response.getOutputStream(),
							result);
				} else {
					response.sendError(204);
				}
			} else {
				mapper.writeValue(response.getOutputStream(),
						user.getCart().getItems());
			}
		} else {
			response.sendError(204);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String code = request.getParameter("code");
		String action = request.getParameter("action");
		String services = request.getParameter("services");
		User user = (User) request.getSession().getAttribute("user");
		if(user == null || user.getRole() != User.Role.CUSTOMER) {
			response.sendError(401);
			return;
		}
		
		response.setContentType("application/text");
		
		FurnitureItemCollection fic = (FurnitureItemCollection) getServletContext().getAttribute("items");
		FurnitureItem product = fic.getActiveItem(code);
		
		if(product != null) {
			if(action != null) {
				switch(action) {
				case "n":
					ShoppingCartItem newItem = new ShoppingCartItem(product, 1);
					if(services != null) {
						AdditionalServiceCollection asc = (AdditionalServiceCollection) getServletContext().getAttribute("services");
						String[] names = services.split("\n");
						for(int i=0; i<names.length; i++) {
							AdditionalService as = asc.getService(names[i]);
							if(as != null && as.getItem().getCode().equals(code)) {
								newItem.addService(as);
							}
						}
					}
					user.getCart().addToShoppingCart(newItem);
					break;
				case "e":
					ShoppingCartItem editedItem = new ShoppingCartItem(product, 1);
					if(services != null) {
						AdditionalServiceCollection asc = (AdditionalServiceCollection) getServletContext().getAttribute("services");
						String[] names = services.split("\n");
						for(int i=0; i<names.length; i++) {
							AdditionalService as = asc.getService(names[i]);
							if(as != null && as.getItem().getCode().equals(code)) {
								editedItem.addService(as);
							}
						}
					}
					user.getCart().editItem(editedItem);
					break;
				case "+":
					ShoppingCartItem sci = user.getCart().getItemByCode(code);
					if(sci != null) {
						sci.setCount(sci.getCount() + 1);
					}
					//user.getCart().addToShoppingCart(new ShoppingCartItem(product, 1));
					break;
				case "-":
					ShoppingCartItem scitem = user.getCart().getItemByCode(code);
					if(scitem != null && scitem.getCount() > 1) {
						scitem.setCount(scitem.getCount() - 1);
					}
					break;
				case "x":
					user.getCart().removeFromShoppingCart(new ShoppingCartItem(product, 1));
					break;
				
				}
			}
			//user.getCart().addToShoppingCart(new ShoppingCartItem(product, 1));
		} else {
			response.sendError(204);
		}
		
		request.getSession().setAttribute("user", user);
	}

}
