package servlets;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.AdditionalServiceCollection;
import model.Discount;
import model.DiscountCollection;
import model.FurnitureItemCollection;
import model.FurnitureShowroomCollection;
import model.FurnitureTypeCollection;
import model.User;

import org.codehaus.jackson.map.ObjectMapper;

/**
 * Servlet implementation class DiscountsServlet
 */
public class DiscountsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DiscountsServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		User currentUser = (User) request.getSession().getAttribute("user");
		if(currentUser == null) {
			response.sendError(204);
			return;
		}
		
		String id = request.getParameter("id");
		response.setContentType("application/json");
		
		DiscountCollection discounts = (DiscountCollection) getServletContext().getAttribute("discounts");
		
		ObjectMapper mapper = new ObjectMapper();
		if(id == null) {
			mapper.writeValue(response.getOutputStream(), discounts.getDiscounts());
			System.out.println(discounts.getDiscounts().size());
		} else {
			mapper.writeValue(response.getOutputStream(), discounts.getDiscount(Integer.parseInt(id)));
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		User currentUser = (User) request.getSession().getAttribute("user");
		if(currentUser == null || currentUser.getRole() == User.Role.CUSTOMER) {
			response.sendError(401);
			return;
		}
		
		String start = request.getParameter("start");
		String finish = request.getParameter("finish");
		String percent = request.getParameter("percent");
		String it = request.getParameter("it");
		String item = request.getParameter("item");
		String type = request.getParameter("type");
		String showroom = request.getParameter("showroom");
		
		System.out.println(Discount.getSeed());
		System.out.println(start + "," + finish + "," + it + "," + item + "," + type + "," + showroom); 
		
		FurnitureItemCollection fic = (FurnitureItemCollection) getServletContext().getAttribute("items");
		FurnitureTypeCollection ftc = (FurnitureTypeCollection) getServletContext().getAttribute("categories");
		FurnitureShowroomCollection fsc = (FurnitureShowroomCollection) getServletContext().getAttribute("showrooms");
		
		Discount d = null;
		try {
			d = new Discount(sdf.parse(start), sdf.parse(finish), Double.parseDouble(percent), Integer.parseInt(it), fic.getActiveItem(item), ftc.getActiveType(type), fsc.getShowroom(showroom));
		} catch (NumberFormatException | ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		DiscountCollection discounts = (DiscountCollection) getServletContext().getAttribute("discounts");
		discounts.addDiscount(d);
		getServletContext().setAttribute("discounts", discounts); 
	}

}
