package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;

import model.AdditionalService;
import model.AdditionalServiceCollection;
import model.FurnitureItem;
import model.FurnitureItemCollection;
import model.User;

/**
 * Servlet implementation class ServicesServlet
 */
public class ServicesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ServicesServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		User currentUser = (User) request.getSession().getAttribute("user");
		if (currentUser == null) {
			// response.sendRedirect("login.html");
			response.sendError(204);
			return;
		}

		String name = request.getParameter("name");
		String code = request.getParameter("code");
		response.setContentType("application/json");

		AdditionalServiceCollection services = (AdditionalServiceCollection) getServletContext()
				.getAttribute("services");

		ObjectMapper mapper = new ObjectMapper();
		if (name != null) {
			mapper.writeValue(response.getOutputStream(),
					services.getService(name));
		} else if (code != null) {
			mapper.writeValue(response.getOutputStream(),
					services.getServicesFor(code));
		} else {
			mapper.writeValue(response.getOutputStream(),
					services.getServices());
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		User currentUser = (User) request.getSession().getAttribute("user");
		if (currentUser == null || currentUser.getRole() != User.Role.ADMIN) {
			response.sendError(401);
			return;
		}

		String name = request.getParameter("name");
		String desc = request.getParameter("desc");
		String code = request.getParameter("code");
		String price = request.getParameter("price");
		String mod = request.getParameter("mod");

		AdditionalServiceCollection services = (AdditionalServiceCollection) getServletContext()
				.getAttribute("services");
		FurnitureItemCollection fic = (FurnitureItemCollection) getServletContext()
				.getAttribute("items");

		/*
		 * if(services.getService(name) != null) { if(desc != null) {
		 * AdditionalService oldService = services.getService(name);
		 * oldService.setDescription(desc);
		 * oldService.setItem(fic.getActiveItem(code)); } else {
		 * services.deleteService(name); } } else { AdditionalService as = new
		 * AdditionalService(name, desc, 22.5, fic.getActiveItem(code));
		 * services.addService(as); }
		 */
		if (mod == null) {
			response.sendError(204);
			return;
		}

		switch (mod) {
		case "+":
			double pr = Double.parseDouble(price);
			FurnitureItem item = fic.getActiveItem(code);
			if(name != null && desc != null && price != null && item != null) {
				AdditionalService newService = new AdditionalService(name, desc, pr, item);
				if(!services.addService(newService)) {
					response.sendError(204);
					return;
				}
			} else {
				response.sendError(204);
				return;
			}
			break;
		case "e":
			double prx = Double.parseDouble(price);
			FurnitureItem itm = fic.getActiveItem(code);
			if(name != null && desc != null && price != null && itm != null) {
				AdditionalService service = new AdditionalService(name, desc, prx, itm);
				if(!services.setService(service)) {
					response.sendError(204);
					return;
				}
			} else {
				response.sendError(204);
				return;
			}
			break;
		case "-":
			if(name != null) {
				if(!services.deleteService(name)) {
					response.sendError(204);
					return;
				}
			}
			break;

		default:
			response.sendError(204);
			return;
		}

		getServletContext().setAttribute("services", services);

		ObjectMapper mapper = new ObjectMapper();
		mapper.writeValue(response.getOutputStream(), services.getServices());
	}

}
