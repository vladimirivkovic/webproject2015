package servlets;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.*;

import org.codehaus.jackson.map.ObjectMapper;

/**
 * Servlet implementation class CategoryReportServlet
 */
public class CategoryReportServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CategoryReportServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		User currentUser = (User) request.getSession().getAttribute("user");
		if(currentUser == null || currentUser.getRole() != User.Role.MANAGER) {
			response.sendError(401);
			return;
		}
		
		
		String start = request.getParameter("start");
		String stop = request.getParameter("stop");
		String type = request.getParameter("category");
		
		if(start != null && stop != null && type != null) {
			try {
				BillCollection ac = (BillCollection) getServletContext().getAttribute("bills");
				FurnitureTypeCollection ftc = (FurnitureTypeCollection) getServletContext()
						.getAttribute("categories");
				
				FurnitureType category = ftc.getType(type);
				if(category == null) {
					response.sendError(204);
					return;
				}
				
				Date first = sdf.parse(start);
				Date last = sdf.parse(stop);
				
				CategoryReportItem report = new CategoryReportItem();
				
				
				for (Bill a : ac.getBills()) {
					Date d = a.getDateAndTime();
					if(first.before(d) && last.after(d)) {
						for (ShoppingCartItem item : a.getItems()) {
							FurnitureType itemType = item.getProduct().getType();
							if(category.equals(itemType) || ftc.areLinked(category, itemType)) {
								System.out.println(item.getCount() + " x " + item.getProduct().getName());
								report.addItem(item.getProduct().getCode(), item.getCount());
							}
						}
					}
				}
				
				response.setContentType("application/json");
				ObjectMapper mapper = new ObjectMapper();
				mapper.writeValue(response.getOutputStream(), report.getSoldItems().values());
			} catch (Exception e) {
				response.sendError(204);
				e.printStackTrace();
			}
		}
	}

}
