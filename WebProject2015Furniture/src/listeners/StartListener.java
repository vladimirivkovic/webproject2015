package listeners;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import model.*;

import org.codehaus.jackson.*;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.sun.org.apache.bcel.internal.generic.NEW;

import serialization.*;

/**
 * Application Lifecycle Listener implementation class StartListener
 *
 */
public class StartListener implements ServletContextListener {

	/**
	 * Default constructor.
	 */
	public StartListener() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see ServletContextListener#contextInitialized(ServletContextEvent)
	 */
	public void contextInitialized(ServletContextEvent ctx) {
		String realPath = ctx.getServletContext().getRealPath("/");
		
		readUsers(realPath, ctx);

		readCategories(realPath, ctx);

		readShowrooms(realPath, ctx);

		readItems(realPath, ctx);

		readServices(realPath, ctx);
		
		readBills(realPath, ctx);
		
		readCarts(realPath, ctx);
		
		ArrayList<FurnitureType> archievedTypes = new ArrayList<FurnitureType>();
		ctx.getServletContext().setAttribute("archievedTypes", archievedTypes);

		try {
			readDiscounts(realPath, ctx);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @see ServletContextListener#contextDestroyed(ServletContextEvent)
	 */
	public void contextDestroyed(ServletContextEvent ctx) {
		try {
			String realPath = ctx.getServletContext().getRealPath("/");
			
			writeTypes(realPath, ctx);
			
			writeCarts(realPath, ctx);
			
			writeItems(realPath, ctx);
			
			writeDiscounts(realPath, ctx);
			
			writeServices(realPath, ctx);
			
			writeBills(realPath, ctx);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void readCategories(String realPath, ServletContextEvent ctx) {
		
		
		
		try {
			ObjectMapper mapper = new ObjectMapper();
			TypeWrapperCollection typeWrapperCollection = mapper.readValue(new File(realPath
						+ "data/categories.json"), TypeWrapperCollection.class);
			
			FurnitureTypeCollection types = typeWrapperCollection.makeCollection();
			ctx.getServletContext().setAttribute("categories", types);
			
			for(FurnitureType ft : types.getActiveTypes()) {
				System.out.println(ft.getName());
			}
		} catch (JsonGenerationException e) {

			e.printStackTrace();

		} catch (JsonMappingException e) {

			e.printStackTrace();

		} catch (IOException e) {

			e.printStackTrace();

		}
	}

	private void readShowrooms(String realPath, ServletContextEvent ctx) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			ShowroomCollectionWrapper showrooms = mapper.readValue(new File(realPath
						+ "data/showrooms.json"), ShowroomCollectionWrapper.class);
			
			FurnitureShowroomCollection fsc = showrooms.getCollection();
			ctx.getServletContext().setAttribute("showrooms", fsc);
			
			for(FurnitureShowroom ft : fsc.getShowrooms()) {
				System.out.println(ft.getName());
			}
		} catch (JsonGenerationException e) {

			e.printStackTrace();

		} catch (JsonMappingException e) {

			e.printStackTrace();

		} catch (IOException e) {

			e.printStackTrace();

		}
	}

	private void readItems(String realPath, ServletContextEvent ctx) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			ItemWrapperCollection items = mapper.readValue(new File(realPath
						+ "data/items.json"), ItemWrapperCollection.class);
			
			FurnitureShowroomCollection showrooms = (FurnitureShowroomCollection) 
					ctx.getServletContext().getAttribute("showrooms");
			FurnitureTypeCollection types = (FurnitureTypeCollection) 
					ctx.getServletContext().getAttribute("categories");
			FurnitureItemCollection fic = items.makeCollection(types, showrooms);
			ctx.getServletContext().setAttribute("items", fic);
			
			for(FurnitureItem ft : fic.getItems()) {
				System.out.println(ft.getName());
			}

		} catch (JsonGenerationException e) {

			e.printStackTrace();

		} catch (JsonMappingException e) {

			e.printStackTrace();

		} catch (IOException e) {

			e.printStackTrace();

		}
	}

	private void readServices(String realPath, ServletContextEvent ctx) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			ServicesWrapperCollection services = mapper.readValue(new File(realPath
						+ "data/services.json"), ServicesWrapperCollection.class);
			
			FurnitureItemCollection fic = (FurnitureItemCollection) 
					ctx.getServletContext().getAttribute("items");
			AdditionalServiceCollection asc = services.makeCollection(fic);
			
			ctx.getServletContext().setAttribute("services", asc);
			
			for(AdditionalService ft : asc.getServices()) {
				System.out.println(ft.getName());
			}
			

		} catch (JsonGenerationException e) {

			e.printStackTrace();

		} catch (JsonMappingException e) {

			e.printStackTrace();

		} catch (IOException e) {

			e.printStackTrace();

		}
	}

	private void readDiscounts(String realPath, ServletContextEvent ctx)
			throws ParseException {
		try {
			ObjectMapper mapper = new ObjectMapper();
			DiscountWrapperCollection dicounts = mapper.readValue(new File(realPath
						+ "data/discounts.json"), DiscountWrapperCollection.class);
			
			FurnitureShowroomCollection showrooms = (FurnitureShowroomCollection) 
					ctx.getServletContext().getAttribute("showrooms");
			FurnitureTypeCollection types = (FurnitureTypeCollection) 
					ctx.getServletContext().getAttribute("categories");
			FurnitureItemCollection fic = (FurnitureItemCollection) 
					ctx.getServletContext().getAttribute("items");
			DiscountCollection dc = dicounts.makeCollection(fic, types, showrooms);
			ctx.getServletContext().setAttribute("discounts", dc);
			
			for(Discount ft : dc.getDiscounts()) {
				System.out.println(ft.getPercent());
			}
			int max = 0;
			for(Discount d : dc.getDiscounts()) {
				if(d.getId() > max) {
					max = d.getId();
				}
			}
			Discount.setSeed(max+1);
			

		} catch (JsonGenerationException e) {

			e.printStackTrace();

		} catch (JsonMappingException e) {

			e.printStackTrace();

		} catch (IOException e) {

			e.printStackTrace();

		}
	}

	private void readUsers(String realPath, ServletContextEvent ctx) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			UserWrapperCollection users = mapper.readValue(new File(realPath
						+ "data/users.json"), UserWrapperCollection.class);
			
			UsersCollection uc = users.getCollection();
			ctx.getServletContext().setAttribute("users", uc);
			
			for(User ft : uc.getUsers()) {
				System.out.println(ft.getName());
			}

		} catch (JsonGenerationException e) {

			e.printStackTrace();

		} catch (JsonMappingException e) {

			e.printStackTrace();

		} catch (IOException e) {

			e.printStackTrace();

		}
	}
	
	private void readCarts(String realPath, ServletContextEvent ctx) {
		
		try {
			UsersCollection uc = (UsersCollection) 
					ctx.getServletContext().getAttribute("users");
			FurnitureItemCollection fic = (FurnitureItemCollection) 
					ctx.getServletContext().getAttribute("items");
			AdditionalServiceCollection adc = (AdditionalServiceCollection) 
					ctx.getServletContext().getAttribute("services");
			
			ObjectMapper mapper = new ObjectMapper();
			ShoppingCartWrapperCollection carts = mapper.readValue(new File(realPath
						+ "data/carts.json"), ShoppingCartWrapperCollection.class);
			
			carts.setCartsToUsers(uc, fic,  adc);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	private void readBills(String realPath, ServletContextEvent ctx) {
		
		try {
			UsersCollection uc = (UsersCollection) 
					ctx.getServletContext().getAttribute("users");
			FurnitureItemCollection fic = (FurnitureItemCollection) 
					ctx.getServletContext().getAttribute("items");
			AdditionalServiceCollection adc = (AdditionalServiceCollection) 
					ctx.getServletContext().getAttribute("services");
			
			ObjectMapper mapper = new ObjectMapper();
			BillWrapperCollection bills = mapper.readValue(new File(realPath
						+ "data/bills.json"), BillWrapperCollection.class);
			
			BillCollection bc = bills.makeCollection(uc, fic, adc);
			int max = 0;
			for(Bill b : bc.getBills()) {
				if(b.getId() > max) {
					max = b.getId();
				}
			}
			Bill.setSeed(max+1);
			ctx.getServletContext().setAttribute("bills", bc);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}


	
	private void writeTypes(String realPath, ServletContextEvent ctx) {
		
		try {
			ObjectMapper mapper = new ObjectMapper();
			FurnitureTypeCollection ftc = (FurnitureTypeCollection) 
					ctx.getServletContext().getAttribute("categories");
			mapper.writeValue(new File(realPath + "data/categories.json"), new TypeWrapperCollection(ftc));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void writeCarts(String realPath, ServletContextEvent ctx) {
		
		try {
			ObjectMapper mapper = new ObjectMapper();
			UsersCollection users = (UsersCollection) 
					ctx.getServletContext().getAttribute("users");
			mapper.writeValue(new File(realPath + "data/carts.json"), new ShoppingCartWrapperCollection(users));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void writeItems(String realPath, ServletContextEvent ctx) {
		
		try {
			ObjectMapper mapper = new ObjectMapper();
			FurnitureItemCollection items = (FurnitureItemCollection) 
					ctx.getServletContext().getAttribute("items");
			mapper.writeValue(new File(realPath + "data/items.json"), new ItemWrapperCollection(items));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void writeServices(String realPath, ServletContextEvent ctx) {
		
		try {
			ObjectMapper mapper = new ObjectMapper();
			AdditionalServiceCollection services = (AdditionalServiceCollection) 
					ctx.getServletContext().getAttribute("services");
			mapper.writeValue(new File(realPath + "data/services.json"), new ServicesWrapperCollection(services));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void writeDiscounts(String realPath, ServletContextEvent ctx) {
		
		try {
			ObjectMapper mapper = new ObjectMapper();
			DiscountCollection discounts = (DiscountCollection) 
					ctx.getServletContext().getAttribute("discounts");
			mapper.writeValue(new File(realPath + "data/discounts.json"), new DiscountWrapperCollection(discounts));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void writeBills(String realPath, ServletContextEvent ctx) {
		
		try {
			ObjectMapper mapper = new ObjectMapper();
			BillCollection bills = (BillCollection) 
					ctx.getServletContext().getAttribute("bills");
			mapper.writeValue(new File(realPath + "data/bills.json"), new BillWrapperCollection(bills));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
