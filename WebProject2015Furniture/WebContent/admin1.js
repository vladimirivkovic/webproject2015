var $tabHome = $("<li></li>").attr("class", "active").attr("id", "home-tab");
$tabHome.append($("<a></a>").attr("href", "#").text("Home"));
$("#tabs").append($tabHome);
var $tabCategories = $("<li></li>").attr("id", "categories-tab");
$tabCategories.append($("<a></a>").attr("href", "#").text("Categories"));
$("#tabs").append($tabCategories);
var $tabShowrooms = $("<li></li>").attr("id", "showrooms-tab");
$tabShowrooms.append($("<a></a>").attr("href", "#").text("Showrooms"));
$("#tabs").append($tabShowrooms);
var $tabProducts = $("<li></li>").attr("id", "products-tab");
$tabProducts.append($("<a></a>").attr("href", "#").text("Products"));
$("#tabs").append($tabProducts);
var $tabServices = $("<li></li>").attr("id", "services-tab");
$tabServices.append($("<a></a>").attr("href", "#").text("Additional services"));
$("#tabs").append($tabServices);
var $tabDiscounts = $("<li></li>").attr("id", "discounts-tab");
$tabDiscounts.append($("<a></a>").attr("href", "#").text("Discounts"));
$("#tabs").append($tabDiscounts);
	
	var updateTree = function() {
	$("#categories-tree").empty();
	
		$.get("categories", {name : ""}, function(data) {
			var $root = $("<li></li>");
			var $rootlbl = $("<label></label>")
			.attr("class", "tree-toggle nav-header")
			.text("All categories");
			$rootlbl.click(nodeClick);
			$root.append($rootlbl);
			$("#categories-tree").append($root);
			var $rootlst = $("<ul></ul>").attr("class", "nav nav-list tree");
			$root.append($rootlst);
			var $lst = $rootlst;
			$("#select-category-modal").empty();
			$("#select-supercategory-modal").empty();
			//alert(data);
			$("#select-category-modal").append($("<option></option>").text("-"));
			$("#select-supercategory-modal").append($("<option></option>").text("-"));
			$("#select-category-modal-edit").append($("<option></option>").text("-"));
			$("#select-supercategory-modal-edit").append($("<option></option>").text("-"));
			for(var i=0; i<data.length; i++) {
				$("#select-category-modal").append($("<option></option>").text(data[i].name));
				$("#select-supercategory-modal").append($("<option></option>").text(data[i].name));
				$("#select-category-modal-edit").append($("<option></option>").text(data[i].name));
				$("#select-supercategory-modal-edit").append($("<option></option>").text(data[i].name));
				if(data[i].superType == null) {
					var $item = $("<li></li>");
					var $lbl = $("<label></label>")
					.attr("class", "tree-toggle nav-header")
					.text(data[i].name);
					$lbl.click(nodeClick);
					$item.append($lbl);
					dfs($item, data, data[i].name)
					$lst.append($item);
				}
			}
		});
	};
	
	var dfs = function($node, data, name) {
		var x = [];
		for(var i=0; i<data.length; i++) {
			if(data[i].superType != null) if(data[i].superType.name == name) {
				x.push(data[i]);
			}
		}
		
		if(x.length) {
			var $lst = $("<ul></ul>").attr("class", "nav nav-list tree");
			for(var j=0; j<x.length; j++) {
				var $item = $("<li></li>");
				var $lblx = $("<label></label>")
							.attr("class", "tree-toggle nav-header")
							.text(x[j].name);
				$lblx.click(nodeClick);
				$item.append($lblx);
				dfs($item, data, x[j].name);
				
				$lst.append($item);
			}
			$node.append($lst);
		} else {
			var $t = $node.find("label").first().text();
			$node.empty();
			$node.append($("<a></a>").attr("href", "#").text($t));
		}
	};
	
	//updateTree();
	
	var nodeClick = function () {
		$(this).parent().children('ul.tree').toggle(200);
		lastClicked = $(this).text();
		$("#categories-tree").find("label").css("background-color", "transparent");
		$(this).css("background-color", "yellow");
		loadItems();
	};
	
	var loadCategoriesIn = function($list) {
		$.get("categories", {name : ""}, function(data) {
			$list.empty();
			for(var i=0; i<data.length; i++) {
				$list.append($("<option></option>").text(data[i].name));
			}
		});
	};
	
	var loadShowroomsIn = function($list) {
		$.get("home", function(data, status){
			$list.empty();
			for(var i=0; i<data.length; i++) {
				$list.append($("<option></option>").text(data[i].name).attr("id", data[i].pib));
			}
		});
	};
	
	$.get("categories", {name : ""}, function(data) {
		$("#select-category-modal").append($("<option></option>").text("-"));
		$("#select-supercategory-modal").append($("<option></option>").text("-"));
		$("#select-category-modal-edit").append($("<option></option>").text("-"));
		$("#select-supercategory-modal-edit").append($("<option></option>").text("-"));
		for(var i=0; i<data.length; i++) {
			$("#select-category-modal").append($("<option></option>").text(data[i].name));
			$("#select-supercategory-modal").append($("<option></option>").text(data[i].name));
			$("#select-category-modal-edit").append($("<option></option>").text(data[i].name));
			$("#select-supercategory-modal-edit").append($("<option></option>").text(data[i].name));
		}
	});
	
	$("#new-category").click(function() {
		var name = $("#type-name").val();
		var desc = $("#type-desc").val();
		var spr = $("#select-supercategory-modal").val();
		
		$.post("categories", {name: name, desc: desc, spr: spr}, function(data, status) {
			if(status == "success") {
				var $items = $("#categories-tree").find("li");
				$.each($items, function() {
					var $ch = $(this).children().first();
					if($ch.text() == spr) {
						var $lst = null;
						if($ch.prop("tagName") == "A") {
							$ch = $ch.parent();
							$ch.empty();
							var $lblx = $("<label></label>")
								.attr("class", "tree-toggle nav-header")
								.text(spr);
							$ch.append($lblx);
							$lst = $("<ul></ul>").attr("class", "nav nav-list tree");
							$ch.append($lst);
							$lblx.click(nodeClick);
						} else {
							$lst = $ch.parent().children().last();
						}
						var $item = $("<li></li>");
						$item.append($("<a></a>").attr("href", "#").text(name));
						$lst.append($item);
					}
				});
				alert("New category created");
				loadCategories();
			}
		});
	});
	
	$.get("home", function(data, status){
		for(var i=0; i<data.length; i++) {
			//$("#select-showroom").append($("<option></option>").text(data[i].name).attr("id", data[i].pib));
			$("#select-showroom-modal").append($("<option></option>").text(data[i].name).attr("id", data[i].pib));
			$("#select-showroom-modal-edit").append($("<option></option>").text(data[i].name).attr("id", data[i].pib));
		}
	});
	
	/*$("#select-showroom").change(function() {
		  loadItems();
	});*/
	
	/*var loadItems = function() {
		var lastSelected = $("#select-showroom").find(":selected").attr("id");
		$.post("items", {type: lastClicked, showroom: lastSelected}, function(data) {
			$("#items-list").empty();
			var $hrow = $("<tr></tr>");
			$hrow.append($("<th></th>").text("code"));
			$hrow.append($("<th></th>").text("name"));
			$hrow.append($("<th></th>").text("producer"));
			$hrow.append($("<th></th>").text("price"));
			$hrow.append($("<th></th>").text(" "));
			$hrow.append($("<th></th>").text(" "));
			$("#items-list").append($hrow);
			for(var i=0; i<data.length; i++) {
				var $row = $("<tr></tr>");
				$row.append($("<td></td>").text(data[i].code));
				$row.append($("<td></td>").text(data[i].name));
				$row.append($("<td></td>").text(data[i].producer));
				$row.append($("<td></td>").text(data[i].price));
				
				var $btnedit = $("<button></button>").attr("code", data[i].code);;
				$btnedit.append($("<span></span>").attr("class","glyphicon glyphicon-edit"));
				$row.append($("<td></td>").append($btnedit));
				$btnedit.click(editClick);
				var $btndelete = $("<button></button>").attr("code", data[i].code);
				$btndelete.append($("<span></span>").attr("class","glyphicon glyphicon-remove"));
				$row.append($("<td></td>").append($btndelete));
				$btndelete.click(deleteClick);
				//$row.append('<td><a data-target="#categoryModal"><span class="glyphicon glyphicon-edit"></span></a></td>');
				//$row.append('<td><a><span class="glyphicon glyphicon-remove"></span></a></td>');
				$("#items-list").append($row);
			}
		});
	};*/
	
	
	$("#new-item").click(function() {
		var code = $("#item-code").val();
		var name = $("#item-name").val();
		var type = $("#select-category-modal").val();
		var showroom = $("#select-showroom-modal").find(":selected").attr("id");
		
		$.post(
				"items",
				{
					code: code,
					name: name,
					type: type,
					showroom: showroom
				},
				function(data) {
					loadProducts();
				});
	});
	
	var deleteClick = function() {
		var code = $(this).attr("code");
		var type = $("#select-category-modal").val();
		var showroom = $("#select-showroom-modal").find(":selected").attr("id");
		
		$.post(
				"items",
				{
					code: code
				},
				function() {
					//loadItems();
					loadProducts();
				});
	};
	
	var editClick = function() {
		var code = $(this).attr("code");
		
		$.get("items", {code: code}, function(data) {
			$("#item-code-edit").val(data.code);
			$("#item-name-edit").val(data.name);
			$("#select-showroom-modal-edit").val(data.showroom.name);
			$("#select-category-modal-edit").val(data.type.name);
			$("#editItemModal").modal();
		});
	};
	
	
	
	$("#edit-item").click(function() {
		var code = $("#item-code-edit").val();
		var name = $("#item-name-edit").val();
		var type = $("#select-category-modal-edit").val();
		var showroom = $("#select-showroom-modal-edit").find(":selected").attr("id");
		
		$.post(
				"items",
				{
					code: code,
					name: name,
					type: type,
					showroom: showroom
				},
				function() {
					loadProducts();
				});
	});
	
	var editCategoryClick = function() {
		var name = $(this).attr("name");
		
		$.get("categories", {name: name}, function(data) {
			$("#type-name-edit").val(data.name);
			$("#type-desc-edit").val(data.description);
			if(data.superType)
				$("#select-supercategory-modal-edit").val(data.superType.name);
			else
				$("#select-supercategory-modal-edit").val("-");
			$("#editCategoryModal").modal();
		});
		
		

	};
	
	$("#edit-category").click(function() {
		var name = $("#type-name-edit").val();
		var desc = $("#type-desc-edit").val();
		var spr = $("#select-supercategory-modal-edit").val();
		
		if(spr == "-")
			spr = null;
		
		$.post(
				"categories",
				{
					name: name,
					desc: desc,
					spr: spr
				},
				function() {
					loadCategories();
				});
	});
	
	var loadProductsIn = function($list) {
		$.post("items", {type: "All categories", showroom: "All showrooms"}, function(data) {
			$list.empty();
			for(var i=0; i<data.length; i++) {
				$list.append($("<option></option>").text(data[i].name).attr("code", data[i].code));
			}
		});
	};
	
	
	
	loadProductsIn($("#select-item-modal"));
	
	$("#new-service").click(function() {
		var name = $("#service-name").val();
		var desc = $("#service-desc").val();
		var code = $("#select-item-modal").find(":selected").attr("code");
		
		$.post(
				"services",
				{
					code: code,
					name: name,
					desc: desc
				},
				function(data) {
					loadServices();
				});
	});
	
	var editServiceClick = function() {
		var name = $(this).attr("name");
		loadProductsIn($("#select-item-modal-edit"));
		
		$.get("services", {name: name}, function(data) {
			$("#service-name-edit").val(data.name);
			$("#service-desc-edit").val(data.description);
			var item = $("#select-item-modal-edit").find("option[code='" + data.item.code + "']").text();
			$("#select-item-modal-edit").val(item);
			$("#editServiceModal").modal();
		});
	};
	
	$("#edit-service").click(function() {
		var name = $("#service-name-edit").val();
		var desc = $("#service-desc-edit").val();
		var code = $("#select-item-modal-edit").find(":selected").attr("code");
		
		$.post(
				"services",
				{
					code: code,
					name: name,
					desc: desc
				},
				function(data) {
					loadServices();
				});
	});
	
	var deleteServiceClick = function() {
		var name = $(this).attr("name");
		
		$.post("services", {name: name}, function(data) {
			loadServices();
		});
	};
	
	$("#new-discount").click(function() {
		var start = $("#discount-start").val();
		var finish = $("#discount-finish").val();
		var percent = $("#discount-percent").val();
		var item = $("#select-discount-item-modal").find(":selected").attr("code");
		var type = $("#select-discount-category-modal").val();
		var showroom = $("#select-discount-showroom-modal").find(":selected").attr("id");
		var it = 0;
		
		if($('input[name=optradio]:radio:checked').attr("id") == "radio1") {
			//alert($('input[name=optradio]:radio:checked').attr("id"));
			it = 1;
		}
		
		$.post(
				"discounts",
				{
					start: start,
					finish: finish,
					percent: percent,
					it: it,
					item: item,
					type: type,
					showroom: showroom
				},
				function(data) {
					loadDiscounts();
				});
	});
	
	$("#home-tab").click(function() {
		$tab = $(this);
		$tab.parent().children().removeClass("active");
		$tab.attr("class", "active");
		var $main = $("#main");
		$main.empty();
		var $tree = $("<div></div>").attr("class", "well");
		$main.append($tree);
		$lst = $("<ul></ul>").attr("class", "nav nav-list").attr("id", "categories-tree");
		$tree.append($lst);
		updateTree();
	});
	
	
	
	$("#showrooms-tab").click(function() {
		$tab = $(this);
		$.get("home", function(data, status){
			$tab.parent().children().removeClass("active");
			$tab.attr("class", "active");
			var $main = $("#main");
			$main.empty();
			var $table = $("<table></table>");
			$table.attr("class", "table");
			$main.append($table);
			var $header = $("<tr></tr>");
			$header.append($("<th></th>").text("name"));
			$header.append($("<th></th>").text("address"));
			$header.append($("<th></th>").text("telephone"));
			$header.append($("<th></th>").text("e-mail"));
			$header.append($("<th></th>").text("pib"));
			$table.append($header);
			for(var i=0; i<data.length; i++) {
				var $row = $("<tr></tr>");
				$row.append($("<td></td>").text(data[i].name));
				$row.append($("<td></td>").text(data[i].address));
				$row.append($("<td></td>").text(data[i].telephone));
				$row.append($("<td></td>").text(data[i].email));
				$row.append($("<td></td>").text(data[i].pib));
				$table.append($row);
			}
			
		});
		
	});
	
	$("#categories-tab").click(function() {
		$tab = $(this);
		$tab.parent().children().removeClass("active");
		$tab.attr("class", "active");
		loadCategories();
	});

	$("#products-tab").click(function() {
		$tab = $(this);
		$tab.parent().children().removeClass("active");
		$tab.attr("class", "active");
		loadProducts();
	});
	
	$("#services-tab").click(function() {
		$tab = $(this);
		$tab.parent().children().removeClass("active");
		$tab.attr("class", "active");
		loadServices();
	});
	
	$("#discounts-tab").click(function() {
		$tab = $(this);
		$tab.parent().children().removeClass("active");
		$tab.attr("class", "active");
		loadDiscounts();
	});

	$("#searchButton").click(function() {
		var txt = $("#searchField").val();

		var $table = $("#main").find("tbody").first();
		var pat = new RegExp(txt, "i");
		if($table) {
			$.each($table.children(), function() {
				var hidden = true;
				$.each($(this).children(), function() {
					if($(this).text().search(pat) >= 0) {
						hidden = false;
					}
					if($(this).prop("tagName") == "TH") {
						hidden = false;
					}
				});
				if(hidden) {
					$(this).hide();
				} else {
					$(this).show();
				}
			});
		}
	});
	
	var getDiscount = function(item, discounts) {
		var d = 0;
		for(var x in discounts) {
			if(discounts[x].it == 0) {
				if(discounts[x].item.code == item.code) {
					if(discounts[x].percent > d) {
						d = discounts[x].percent;
					}
				}
			} else {
				if(discounts[x].type.name == item.type.name) {
					if(discounts[x].percent > d) {
						d = discounts[x].percent;
					}
				}
			}
		}
		
		return d;
	}
	
	var loadProducts = function() {
		$.get("discounts", function(data) {
			var discounts = data;
		$.post("items", {type: "All categories", showroom: "All showrooms"}, function(data) {
			var $main = $("#main");
			$main.empty();
			var $addbtn = $("<button></button>").attr("data-target", "#itemModal")
				.attr("type", "button").attr("data-toggle", "modal").attr("class", "btn btn-primary")
				.text("Add Product");
			$main.append($addbtn);
			var $table = $("<table></table>");
			$table.attr("class", "table");
			$main.append($table);
			var $hrow = $("<tr></tr>");
			$hrow.append($("<th></th>").text("code"));
			$hrow.append($("<th></th>").text("name"));
			$hrow.append($("<th></th>").text("manufacturer"));
			$hrow.append($("<th></th>").text("price"));
			$hrow.append($("<th></th>").text("off"));
			$hrow.append($("<th></th>").text("showroom"));
			$hrow.append($("<th></th>").text("category"));
			$hrow.append($("<th></th>").text(" "));
			$hrow.append($("<th></th>").text(" "));
			$table.append($hrow);
			$("#select-item-modal").empty();
			$("#select-item-modal-edit").empty();
			for(var i=0; i<data.length; i++) {
				$("#select-item-modal").append($("<option></option>").text(data[i].name).attr("code", data[i].code));
				$("#select-item-modal-edit").append($("<option></option>").text(data[i].name).attr("code", data[i].code));
				var $row = $("<tr></tr>");
				$row.append($("<td></td>").text(data[i].code));
				$row.append($("<td></td>").text(data[i].name));
				$row.append($("<td></td>").text(data[i].producer));
				$row.append($("<td></td>").text(data[i].price));
				$row.append($("<td></td>").text(getDiscount(data[i], discounts) + "%"));
				$row.append($("<td></td>").text(data[i].showroom.name));
				$row.append($("<td></td>").text(data[i].type.name));
				
				var $btnedit = $("<a></a>").attr("code", data[i].code);;
				$btnedit.append($("<a></a>").attr("class","glyphicon glyphicon-edit"));
				$row.append($("<td></td>").append($btnedit));
				$btnedit.click(editClick);
				var $btndelete = $("<a></a>").attr("code", data[i].code);
				$btndelete.append($("<span></span>").attr("class","glyphicon glyphicon-remove"));
				$row.append($("<td></td>").append($btndelete));
				$btndelete.click(deleteClick);
				
				var $btncart = $("<a></a>").attr("code", data[i].code);
				$btncart.append($("<span></span>").attr("class","glyphicon glyphicon-shopping-cart"));
				$row.append($("<td></td>").append($btncart));
				//$row.append('<td><a data-target="#categoryModal"><span class="glyphicon glyphicon-edit"></span></a></td>');
				//$row.append('<td><a><span class="glyphicon glyphicon-remove"></span></a></td>');
				$table.append($row);
			}
		});
		
		});
	};
	
	var loadCategories = function() {
		$.get("categories", {name : ""}, function(data, status){
			var $main = $("#main");
			$main.empty();
			var $addbtn = $("<button></button>").attr("data-target", "#categoryModal")
				.attr("type", "button").attr("data-toggle", "modal").attr("class", "btn btn-primary")
				.text("Add Category");
			$main.append($addbtn);
			var $table = $("<table></table>");
			$table.attr("class", "table");
			$main.append($table);
			var $header = $("<tr></tr>");
			$header.append($("<th></th>").text("name"));
			$header.append($("<th></th>").text("description"));
			$header.append($("<th></th>").text("super category"));
			$table.append($header);
			for(var i=0; i<data.length; i++) {
				var $row = $("<tr></tr>");
				$row.append($("<td></td>").text(data[i].name));
				$row.append($("<td></td>").text(data[i].description));
				if(data[i].superType != null) {
					$row.append($("<td></td>").text(data[i].superType.name));
				} else {
					$row.append($("<td></td>").text("-"));
				}
				var $btnedit = $("<a></a>").attr("name", data[i].name);
				$btnedit.append($("<span></span>").attr("class","glyphicon glyphicon-edit"));
				$row.append($("<td></td>").append($btnedit));
				$btnedit.click(editCategoryClick);
				var $btndelete = $("<a></a>").attr("code", data[i].name);
				$btndelete.append($("<span></span>").attr("class","glyphicon glyphicon-remove"));
				$row.append($("<td></td>").append($btndelete));
				//$btndelete.click(deleteClick);
				
				$row.append($("<td></td>").text(data[i].email));
				$row.append($("<td></td>").text(data[i].pib));
				$table.append($row);
			}
			
		});
	};
	
	var loadServices = function() {
		$.get("services", function(data, status){
			var $main = $("#main");
			$main.empty();
			var $addbtn = $("<button></button>").attr("data-target", "#serviceModal")
				.attr("type", "button").attr("data-toggle", "modal").attr("class", "btn btn-primary")
				.text("Add Service");
			$main.append($addbtn);
			var $table = $("<table></table>");
			$table.attr("class", "table");
			$main.append($table);
			var $header = $("<tr></tr>");
			$header.append($("<th></th>").text("name"));
			$header.append($("<th></th>").text("description"));
			$header.append($("<th></th>").text("prices"));
			$header.append($("<th></th>").text("product"));
			$header.append($("<th></th>").text(" "));
			$header.append($("<th></th>").text(" "));
			$table.append($header);
			for(var i=0; i<data.length; i++) {
				var $row = $("<tr></tr>");
				$row.append($("<td></td>").text(data[i].name));
				$row.append($("<td></td>").text(data[i].description));
				$row.append($("<td></td>").text(data[i].price));
				$row.append($("<td></td>").text(data[i].item.name));

				var $btnedit = $("<a></a>").attr("name", data[i].name);
				$btnedit.append($("<span></span>").attr("class","glyphicon glyphicon-edit"));
				$row.append($("<td></td>").append($btnedit));
				$btnedit.click(editServiceClick);
				var $btndelete = $("<a></a>").attr("name", data[i].name);
				$btndelete.append($("<span></span>").attr("class","glyphicon glyphicon-remove"));
				$row.append($("<td></td>").append($btndelete));
				$btndelete.click(deleteServiceClick);
				
				$row.append($("<td></td>").text(data[i].email));
				$row.append($("<td></td>").text(data[i].pib));
				$table.append($row);
			}
			
		});
	};
	
	var loadDiscounts = function() {
		$.get("discounts", function(data, status){
			var $main = $("#main");
			$main.empty();
			loadProductsIn($("#select-discount-item-modal"));
			loadCategoriesIn($("#select-discount-category-modal"));
			loadShowroomsIn($("#select-discount-showroom-modal"));
			var $addbtn = $("<button></button>").attr("data-target", "#discountModal")
				.attr("type", "button").attr("data-toggle", "modal").attr("class", "btn btn-primary")
				.text("Add Discount");
			$main.append($addbtn);
			var $table = $("<table></table>");
			$table.attr("class", "table");
			$main.append($table);
			var $header = $("<tr></tr>");
			$header.append($("<th></th>").text("showroom"));
			$header.append($("<th></th>").text("product"));
			$header.append($("<th></th>").text("category"));
			$header.append($("<th></th>").text("percent"));
			$header.append($("<th></th>").text("starts"));
			$header.append($("<th></th>").text("expries"));
			$table.append($header);
			for(var i=0; i<data.length; i++) {
				var $row = $("<tr></tr>");
				if(data[i].it == "0") {
					$row.append($("<td></td>").text(data[i].item.showroom.name));
					$row.append($("<td></td>").text(data[i].item.name));
					$row.append($("<td></td>").text("-"));
				} else {
					$row.append($("<td></td>").text(data[i].showroom.name));
					$row.append($("<td></td>").text("-"));
					$row.append($("<td></td>").text(data[i].type.name));
				}
				$row.append($("<td></td>").text(data[i].percent + "%"));
				var start = new Date(data[i].start);
				var finish = new Date(data[i].finish);
				$row.append($("<td></td>").text(start.getDate()+"."+ (start.getMonth()+1) +"."+start.getFullYear()));
				$row.append($("<td></td>").text(finish.getDate()+"."+ (finish.getMonth()+1) +"."+finish.getFullYear()));

				/*var $btnedit = $("<a></a>").attr("name", data[i].name);
				$btnedit.append($("<span></span>").attr("class","glyphicon glyphicon-edit"));
				$row.append($("<td></td>").append($btnedit));
				$btnedit.click(editServiceClick);
				var $btndelete = $("<a></a>").attr("name", data[i].name);
				$btndelete.append($("<span></span>").attr("class","glyphicon glyphicon-remove"));
				$row.append($("<td></td>").append($btndelete));
				$btndelete.click(deleteServiceClick);*/

				$table.append($row);
			}
			
		});
	};
	
	

