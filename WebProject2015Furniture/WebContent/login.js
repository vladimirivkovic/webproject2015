$(document).ready(function() {
	$.post("login", function(data) {
		if(data) {
			window.location.replace("index.html");
		}
	});
	
	$("button").click(function(e) {
		e.preventDefault();
		
		var user = $("#inputUsername").val();
		var pass = $("#inputPassword").val();
		
		$.post("login", {username: user, password: pass}, function(data) {
			if(data) {
				window.location.replace("index.html");
			} else {
				$.toaster({ priority : 'danger', title : 'Error', message : 'Wrong username or password'});
			}
		});
		
	});
});