$(document).ready(function() {
	
	$.getScript("loadFunctions.js");
	$("#modals").append($("<div></div").load("modals/NewProduct.html", 
		function() {
			$.getScript("modals/NewProduct.js");
		}
	));
	$("#modals").append($("<div></div").load("modals/EditProduct.html",
		function() {
			$.getScript("modals/EditProduct.js");
		}
	));
	$("#modals").append($("<div></div").load("modals/NewCategory.html",
		function() {
			$.getScript("modals/NewCategory.js");
		}
	));
	$("#modals").append($("<div></div").load("modals/EditCategory.html",
		function() {
			$.getScript("modals/EditCategory.js");
		}
	));
	$("#modals").append($("<div></div").load("modals/NewService.html",
		function() {
			$.getScript("modals/NewService.js");
		}
	));
	$("#modals").append($("<div></div").load("modals/EditService.html",
		function() {
			$.getScript("modals/EditService.js");
		}
	));
	$("#modals").append($("<div></div").load("modals/NewDiscount.html",
		function() {
			$.getScript("modals/NewDiscount.js");
		}
	));
	$("#modals").append($("<div></div").load("modals/DayReport.html"));
	$("#modals").append($("<div></div").load("modals/DayReportTable.html"));
	$("#modals").append($("<div></div").load("modals/CategoryReport.html",
		function() {
			$.getScript("modals/Reports.js");
		}
	));
	$("#searchPanel").append($("<div></div").load("searchPanel.html",
		function() {
			$.getScript("searchPanel.js");
		}
	));
	
	
	var updateTree = function() {
	$("#categories-tree").empty();
	
		$.get("categories", {name : ""}, function(data) {
			var $root = $("<li></li>");
			var $rootlbl = $("<label></label>")
			.attr("class", "tree-toggle nav-header")
			.text("All categories");
			$rootlbl.click(nodeClick);
			$root.append($rootlbl);
			$("#categories-tree").append($root);
			var $rootlst = $("<ul></ul>").attr("class", "nav nav-list tree");
			$root.append($rootlst);
			var $lst = $rootlst;
			$("#select-category-modal").empty();
			$("#select-supercategory-modal").empty();
			//alert(data);
			$("#select-category-modal").append($("<option></option>").text("-"));
			$("#select-supercategory-modal").append($("<option></option>").text("-"));
			$("#select-category-modal-edit").append($("<option></option>").text("-"));
			$("#select-supercategory-modal-edit").append($("<option></option>").text("-"));
			for(var i=0; i<data.length; i++) {
				$("#select-category-modal").append($("<option></option>").text(data[i].name));
				$("#select-supercategory-modal").append($("<option></option>").text(data[i].name));
				$("#select-category-modal-edit").append($("<option></option>").text(data[i].name));
				$("#select-supercategory-modal-edit").append($("<option></option>").text(data[i].name));
				if(data[i].superType == null) {
					var $item = $("<li></li>");
					var $lbl = $("<label></label>")
					.attr("class", "tree-toggle nav-header")
					.text(data[i].name);
					$lbl.click(nodeClick);
					$item.append($lbl);
					dfs($item, data, data[i].name)
					$lst.append($item);
				}
			}
		});
	};
	
	var dfs = function($node, data, name) {
		var x = [];
		for(var i=0; i<data.length; i++) {
			if(data[i].superType != null) if(data[i].superType.name == name) {
				x.push(data[i]);
			}
		}
		
		if(x.length) {
			var $lst = $("<ul></ul>").attr("class", "nav nav-list tree");
			for(var j=0; j<x.length; j++) {
				var $item = $("<li></li>");
				var $lblx = $("<label></label>")
							.attr("class", "tree-toggle nav-header")
							.text(x[j].name);
				$lblx.click(nodeClick);
				$item.append($lblx);
				dfs($item, data, x[j].name);
				
				$lst.append($item);
			}
			$node.append($lst);
		} else {
			var $t = $node.find("label").first().text();
			$node.empty();
			$node.append($("<a></a>").attr("href", "#").text($t));
		}
	};
	
	//updateTree();
	
	var nodeClick = function () {
		$(this).parent().children('ul.tree').toggle(200);
		lastClicked = $(this).text();
		$("#categories-tree").find("label").css("background-color", "transparent");
		$(this).css("background-color", "yellow");
	};
	
	 loadCategoriesIn = function($list) {
		$.get("categories", {name : ""}, function(data) {
			$list.empty();
			for(var i=0; i<data.length; i++) {
				$list.append($("<option></option>").text(data[i].name));
			}
		});
	};
	
	 loadShowroomsIn = function($list) {
		$.get("home", function(data, status){
			$list.empty();
			for(var i=0; i<data.length; i++) {
				$list.append($("<option></option>").text(data[i].name).attr("id", data[i].pib));
			}
		});
	};

	
	deleteClick = function() {
		var code = $(this).attr("code");
		var type = $("#select-category-modal").val();
		var showroom = $("#select-showroom-modal").find(":selected").attr("id");
		
		$.post(
				"items",
				{
					code: code
				},
				function() {
					//loadItems();
					loadProducts();
				});
	};
	
	 loadProductsIn = function($list) {
		$.post("items", {type: "All categories", showroom: "All showrooms"}, function(data) {
			$list.empty();
			for(var i=0; i<data.length; i++) {
				$list.append($("<option></option>").text(data[i].name).attr("code", data[i].code));
			}
		});
	};
	
	
	
	loadProductsIn($("#select-item-modal"));
	
	deleteCategoryClick = function() {
		var name = $(this).attr("name");
		
		$.post("categories", {name: name}, function(data, status) {
			if(status == "nocontent") {
				alert("Only empty category can be deleted");
			} else {
				loadCategories();
			}
		});
	};
	
	deleteServiceClick = function() {
		var name = $(this).attr("name");
		
		$.post("services", {name: name}, function(data) {
			loadServices();
		});
	};
	
	
	
	$("#home-tab").click(function() {
		$tab = $(this);
		$tab.parent().children().removeClass("active");
		$tab.attr("class", "active");
		var $main = $("#main");
		$main.empty();
		var $tree = $("<div></div>").attr("class", "well");
		$main.append($tree);
		$lst = $("<ul></ul>").attr("class", "nav nav-list").attr("id", "categories-tree");
		$tree.append($lst);
		updateTree();
	});
	
	
	
	$("#showrooms-tab").click(function() {
		$tab = $(this);
		$.get("home", function(data, status){
			$("#searchPanel").hide();
			$tab.parent().children().removeClass("active");
			$tab.attr("class", "active");
			var $main = $("#main");
			$("#search-panel").remove();
			$main.empty();
			var $table = $("<table></table>");
			$table.attr("class", "table");
			$main.append($table);
			var $header = $("<tr></tr>");
			$header.append($("<th></th>").text("name"));
			$header.append($("<th></th>").text("address"));
			$header.append($("<th></th>").text("telephone"));
			$header.append($("<th></th>").text("e-mail"));
			$header.append($("<th></th>").text("pib"));
			$table.append($header);
			for(var i=0; i<data.length; i++) {
				var $row = $("<tr></tr>");
				$row.append($("<td></td>").text(data[i].name));
				$row.append($("<td></td>").text(data[i].address));
				$row.append($("<td></td>").text(data[i].telephone));
				$row.append($("<td></td>").text(data[i].email));
				$row.append($("<td></td>").text(data[i].pib));
				$table.append($row);
			}
			
		});
		
	});
	
	$("#categories-tab").click(function() {
		$tab = $(this);
		$tab.parent().children().removeClass("active");
		$tab.attr("class", "active");
		loadCategories();
	});

	$("#products-tab").click(function() {
		$tab = $(this);
		$tab.parent().children().removeClass("active");
		$tab.attr("class", "active");
		loadProducts();
	});
	
	$("#services-tab").click(function() {
		$tab = $(this);
		$tab.parent().children().removeClass("active");
		$tab.attr("class", "active");
		loadServices();
	});
	
	$("#discounts-tab").click(function() {
		$tab = $(this);
		$tab.parent().children().removeClass("active");
		$tab.attr("class", "active");
		loadDiscounts();
	});
	
	$("#accounts-tab").click(function() {
		$tab = $(this);
		$tab.parent().children().removeClass("active");
		$tab.attr("class", "active");
		loadAccounts();
	});

	$("#searchButton").click(function() {
		var txt = $("#searchField").val();

		var $table = $("#main").find("tbody").first();
		var pat = new RegExp(txt, "i");
		if($table) {
			$.each($table.children(), function() {
				var hidden = true;
				$.each($(this).children(), function() {
					if($(this).text().search(pat) >= 0) {
						hidden = false;
					}
					if($(this).prop("tagName") == "TH") {
						hidden = false;
					}
				});
				if(hidden) {
					$(this).hide();
				} else {
					$(this).show();
				}
			});
		}
	});
});
