var somethingChanged = function() {
	var patName = new RegExp($("#search-name").val(), "i");
	var patDesc = new RegExp($("#search-desc").val(), "i");
	var patOrigin = new RegExp($("#search-origin").val(), "i");
	var quantity = $("#search-quantity").val();
	var year = $("#search-year").val();
	var patMan = new RegExp($("#search-manufacturer").val(), "i");
	
	var minPrice = $("#search-minprice").val();
	var maxPrice = $("#search-maxprice").val();
	
	var patCat = $("#search-category").val();
	var patShowroom = $("#search-showroom").val();
	
	
	var isProducts = $("#search-category").is(":visible");
	
	var $rows = $("#main").find("tr");
	if(isProducts) {
		$.each($rows, function() {
			//console.log($(this).prop("tagName"));
			var $name = $(this).children().eq(1);
			var $count = $(this).children().eq(2);
			var $man = $(this).children().eq(3);
			var $price = $(this).children().eq(4);
			var $color = $(this).children().eq(6);
			var $shr = $(this).children().eq(7);
			var $cat = $(this).children().eq(8);
			var $year = $(this).children().eq(9);
			var $country = $(this).children().eq(10);
			var res = 0;
			if($name && ($name.prop("tagName") == "TD")) {
				if($name.text().search(patName) >= 0) {
					res++;
				}
			} else {
				res += 10;
			}
			if($man && ($man.prop("tagName") == "TD")) {
				if($man.text().search(patMan) >=0) {
					res++;
				}
			}
			
			if($country && ($country.prop("tagName") == "TD")) {
				if($country.text().search(patOrigin) >=0) {
					res++;
				}
			}
			
			
			
			if($count && ($count.prop("tagName") == "TD")) {
				if(quantity) {
					if(isNaN($count.text()) || isNaN(quantity)) {
					} else {
						if(Number($count.text()) >= quantity) {
							res++;
						}
					}
				} else {
					res++;
				}
			}
			
			if($year && ($year.prop("tagName") == "TD")) {
				if(year) {
					if(isNaN($year.text()) || isNaN(year)) {
					} else {
						if($year.text() == year) {
							res++;
						}
					}
				} else {
					res++;
				}
			}
			
			if($shr && ($shr.prop("tagName") == "TD")) {
				if($shr.text() == patShowroom || patShowroom == "-") {
					res++;
				}
			}
			if($cat && ($cat.prop("tagName") == "TD")) {
				if($cat.text() == patCat || patCat == "-") {
					res++;
				}
			}
			
			if($price && ($price.prop("tagName") == "TD")) {
				if(minPrice) {
					if(isNaN($price.text()) || isNaN(minPrice)) {
					} else {
						if(Number($price.text()) >= minPrice) {
							res++;
						}
					}
				} else {
					res++;
				}
			}	
			if($price && ($price.prop("tagName") == "TD")) {
				if(maxPrice) {
					if(isNaN($price.text()) || isNaN(maxPrice)) {
					} else {
						if(Number($price.text()) <= maxPrice) {
							res++;
						}
					}
				} else {
					res++;
				}
			}
				
			
			if(res >= 9) {
				$(this).show();
			} else {
				$(this).hide();
			}
		});
	} else {
		$.each($rows, function() {
			var $name = $(this).children().eq(0);
			var $desc = $(this).children().eq(1);
			
			var res = 0;
			if($name && ($name.prop("tagName") == "TD")) {
				if($name.text().search(patName) >= 0) {
					res++;
				}
			} else {
				res += 5;
			}
			if($desc && ($desc.prop("tagName") == "TD")) {
				if($desc.text().search(patDesc) >=0) {
					res++;
				}
			}
			
			if(res >= 2) {
				$(this).show();
			} else {
				$(this).hide();
			}
		});
	}
	
};

$("#search-name").change(somethingChanged);
$("#search-manufacturer").change(somethingChanged);
$("#search-showroom").change(somethingChanged);
$("#search-category").change(somethingChanged);

$("#search-origin").change(somethingChanged);
$("#search-quantity").change(somethingChanged);
$("#search-year").change(somethingChanged);
$("#search-maxprice").change(somethingChanged);
$("#search-minprice").change(somethingChanged);
$("#search-desc").change(somethingChanged);
