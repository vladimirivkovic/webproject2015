$(document).ready(function(){
	$.post("login", function(data) {
		if(data) {
			switch(data[0]) {
			case "A" : 
				//$("body").append($("<script></script>").attr("src", "admin.js"));
				$.getScript("admin.js");
				break;
			case "M" :
				//$("body").append($("<script></script>").attr("src", "manager.js"));
				$.getScript("manager.js");
				break;
			case "C" : 
				//$("body").append($("<script></script>").attr("src", "customer.js"));
				$.getScript("customer.js");
				break;
			default :
				window.location.replace("login.html");
			}
		} else {
			window.location.replace("login.html");
		}
	});
	
	
	$("#logOutButton").click(function() {
		$.post("logout", function() {
			window.location.replace("login.html");
		});
	});
});