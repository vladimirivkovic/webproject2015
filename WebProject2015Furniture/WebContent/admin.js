var $tabHome = $("<li></li>").attr("class", "active").attr("id", "home-tab");
$tabHome.append($("<a></a>").attr("href", "#").text("Home"));
$("#tabs").append($tabHome);
var $tabCategories = $("<li></li>").attr("id", "categories-tab");
$tabCategories.append($("<a></a>").attr("href", "#").text("Categories"));
$("#tabs").append($tabCategories);
var $tabShowrooms = $("<li></li>").attr("id", "showrooms-tab");
$tabShowrooms.append($("<a></a>").attr("href", "#").text("Showrooms"));
$("#tabs").append($tabShowrooms);
var $tabProducts = $("<li></li>").attr("id", "products-tab");
$tabProducts.append($("<a></a>").attr("href", "#").text("Products"));
$("#tabs").append($tabProducts);
var $tabServices = $("<li></li>").attr("id", "services-tab");
$tabServices.append($("<a></a>").attr("href", "#").text("Additional services"));
$("#tabs").append($tabServices);
var $tabDiscounts = $("<li></li>").attr("id", "discounts-tab");
$tabDiscounts.append($("<a></a>").attr("href", "#").text("Discounts"));
$("#tabs").append($tabDiscounts);

$.getScript("loadFunctions.js");
$("#modals").append($("<div></div").load("modals/NewProduct.html", 
	function() {
		$.getScript("modals/NewProduct.js");
	}
));
$("#modals").append($("<div></div").load("modals/EditProduct.html",
	function() {
		$.getScript("modals/EditProduct.js");
	}
));
$("#modals").append($("<div></div").load("modals/NewCategory.html",
	function() {
		$.getScript("modals/NewCategory.js");
	}
));
$("#modals").append($("<div></div").load("modals/EditCategory.html",
	function() {
		$.getScript("modals/EditCategory.js");
	}
));
$("#modals").append($("<div></div").load("modals/NewService.html",
	function() {
		$.getScript("modals/NewService.js");
	}
));
$("#modals").append($("<div></div").load("modals/EditService.html",
	function() {
		$.getScript("modals/EditService.js");
	}
));
$("#modals").append($("<div></div").load("modals/NewDiscount.html",
	function() {
		$.getScript("modals/NewDiscount.js");
	}
));
$("#searchPanel").append($("<div></div").load("searchPanel.html",
	function() {
		$.getScript("searchPanel.js");
	}
));


var updateTree = function() {
$("#categories-tree").empty();

	$.get("categories", {name : ""}, function(data) {
		var $root = $("<li></li>");
		var $rootlbl = $("<label></label>")
		.attr("class", "tree-toggle nav-header")
		.text("All categories");
		$rootlbl.click(nodeClick);
		$root.append($rootlbl);
		$("#categories-tree").append($root);
		var $rootlst = $("<ul></ul>").attr("class", "nav nav-list tree");
		$root.append($rootlst);
		var $lst = $rootlst;
		$("#select-category-modal").empty();
		$("#select-supercategory-modal").empty();
		//alert(data);
		$("#select-category-modal").append($("<option></option>").text("-"));
		$("#select-supercategory-modal").append($("<option></option>").text("-"));
		$("#select-category-modal-edit").append($("<option></option>").text("-"));
		$("#select-supercategory-modal-edit").append($("<option></option>").text("-"));
		for(var i=0; i<data.length; i++) {
			$("#select-category-modal").append($("<option></option>").text(data[i].name));
			$("#select-supercategory-modal").append($("<option></option>").text(data[i].name));
			$("#select-category-modal-edit").append($("<option></option>").text(data[i].name));
			$("#select-supercategory-modal-edit").append($("<option></option>").text(data[i].name));
			if(data[i].superType == null) {
				var $item = $("<li></li>");
				var $lbl = $("<label></label>")
				.attr("class", "tree-toggle nav-header")
				.text(data[i].name);
				$lbl.click(nodeClick);
				$item.append($lbl);
				dfs($item, data, data[i].name)
				$lst.append($item);
			}
		}
	});
};

var dfs = function($node, data, name) {
	var x = [];
	for(var i=0; i<data.length; i++) {
		if(data[i].superType != null) if(data[i].superType.name == name) {
			x.push(data[i]);
		}
	}
	
	if(x.length > -1) {
		var $lst = $("<ul></ul>").attr("class", "nav nav-list tree");
		for(var j=0; j<x.length; j++) {
			var $item = $("<li></li>");
			var $lblx = $("<label></label>")
						.attr("class", "tree-toggle nav-header")
						.text(x[j].name);
			$lblx.click(nodeClick);
			$item.append($lblx);
			dfs($item, data, x[j].name);
			
			$lst.append($item);
		}
		$node.append($lst);
	} else {
		var $t = $node.find("label").first().text();
		$node.empty();
		//$node.append($("<a></a>").attr("href", "#").text($t));
		$node.append($("<label></label>").text($t));
	}
};

var nodeClick = function (e) {
	e.preventDefault();
	$(this).parent().children('ul.tree').toggle(200);
	lastClicked = $(this).text();
	$("#categories-tree").find("label").css("background-color", "transparent");
	$(this).css("background-color", "yellow");
};

deleteClick = function(e) {
	e.preventDefault();
	var code = $(this).attr("code");
	var type = $("#select-category-modal").val();
	var showroom = $("#select-showroom-modal").find(":selected").attr("id");
	
	$.post(
			"items",
			{
				code: code
			},
			function(data, status) {
				if(status == "success")
					$.toaster({ priority : 'success', title : 'Success', message : 'Product deleted'});
				else
					$.toaster({ priority : 'danger', title : 'Error', message : 'Product cannot be deleted'});
				loadProducts(0);
			});
};

deleteCategoryClick = function(e) {
	e.preventDefault();
	var name = $(this).attr("name");
	
	$.post("categories", {name: name, mod: "-"}, function(data, status) {
		if(status == "nocontent") {
			$.toaster({ priority : 'danger', title : 'Error', message : 'Category cannot be deleted'});
		} else {
			$.toaster({ priority : 'success', title : 'Success', message : 'Category deleted'});
			loadCategories(0);
		}
	});
};

deleteServiceClick = function(e) {
	e.preventDefault();
	var name = $(this).attr("name");
	
	$.post("services", {name: name, mod: "-"}, function(data, status) {
		if(status == "success") {
			$.toaster({ priority : 'success', title : 'Success', message : 'Service deleted'});
		} else {
			$.toaster({ priority : 'danger', title : 'Error', message : 'Service cannot be deleted'});
		}
		loadServices(0);
	});
};



$("#home-tab").click(function(e) {
	e.preventDefault();
	$tab = $(this);
	$tab.parent().children().removeClass("active");
	$tab.attr("class", "active");
	var $main = $("#main");
	$main.empty();
	var $tree = $("<div></div>").attr("class", "well");
	$main.append($tree);
	$lst = $("<ul></ul>").attr("class", "nav nav-list")
		.attr("id", "categories-tree");
	$tree.append($lst);
	updateTree();
});



$("#showrooms-tab").click(function() {
	$tab = $(this);
	$.get("home", function(data, status){
		$tab.parent().children().removeClass("active");
		$tab.attr("class", "active");
		var $main = $("#main");
		$("#search-panel").remove();
		$("#searchPanel").hide();
		$main.removeClass("col-sm-8");
		$main.attr("class", "col-sm-12");
		$main.empty();
		var $table = $("<table></table>");
		$table.attr("class", "table");
		$main.append($table);
		var $header = $("<tr></tr>");
		$header.append($("<th></th>").text("name"));
		$header.append($("<th></th>").text("address"));
		$header.append($("<th></th>").text("telephone"));
		$header.append($("<th></th>").text("e-mail"));
		$header.append($("<th></th>").text("pib"));
		$table.append($header);
		for(var i=0; i<data.length; i++) {
			var $row = $("<tr></tr>");
			$row.append($("<td></td>").text(data[i].name));
			$row.append($("<td></td>").text(data[i].address));
			$row.append($("<td></td>").text(data[i].telephone));
			$row.append($("<td></td>").text(data[i].email));
			$row.append($("<td></td>").text(data[i].pib));
			$table.append($row);
		}
		
	});
	
});

$("#categories-tab").click(function(e) {
	e.preventDefault();
	$tab = $(this);
	$tab.parent().children().removeClass("active");
	$tab.attr("class", "active");
	loadCategories(0);
});

$("#products-tab").click(function(e) {
	e.preventDefault();
	$tab = $(this);
	$tab.parent().children().removeClass("active");
	$tab.attr("class", "active");
	loadProducts(0);
});

$("#services-tab").click(function(e) {
	e.preventDefault();
	$tab = $(this);
	$tab.parent().children().removeClass("active");
	$tab.attr("class", "active");
	loadServices(0);
});

$("#discounts-tab").click(function(e) {
	e.preventDefault();
	$tab = $(this);
	$tab.parent().children().removeClass("active");
	$tab.attr("class", "active");
	loadDiscounts(0);
});

$("#accounts-tab").click(function(e) {
	e.preventDefault();
	$tab = $(this);
	$tab.parent().children().removeClass("active");
	$tab.attr("class", "active");
	loadAccounts(0);
});

$("#searchButton").click(function(e) {
	e.preventDefault();
	var txt = $("#searchField").val();

	var $table = $("#main").find("tbody").first();
	var pat = new RegExp(txt, "i");
	if($table) {
		$.each($table.children(), function() {
			var hidden = true;
			$.each($(this).children(), function() {
				if($(this).text().search(pat) >= 0) {
					hidden = false;
				}
				if($(this).prop("tagName") == "TH") {
					hidden = false;
				}
			});
			if(hidden) {
				$(this).hide();
			} else {
				$(this).show();
			}
		});
	}
});