var $tabHome = $("<li></li>").attr("class", "active").attr("id", "home-tab");
$tabHome.append($("<a></a>").attr("href", "#").text("Home"));
$("#tabs").append($tabHome);
var $tabCategories = $("<li></li>").attr("id", "categories-tab");
$tabCategories.append($("<a></a>").attr("href", "#").text("Categories"));
$("#tabs").append($tabCategories);
var $tabShowrooms = $("<li></li>").attr("id", "showrooms-tab");
$tabShowrooms.append($("<a></a>").attr("href", "#").text("Showrooms"));
$("#tabs").append($tabShowrooms);
var $tabProducts = $("<li></li>").attr("id", "products-tab");
$tabProducts.append($("<a></a>").attr("href", "#").text("Products"));
$("#tabs").append($tabProducts);
var $tabServices = $("<li></li>").attr("id", "services-tab");
$tabServices.append($("<a></a>").attr("href", "#").text("Additional services"));
$("#tabs").append($tabServices);
var $tabDiscounts = $("<li></li>").attr("id", "discounts-tab");
$tabDiscounts.append($("<a></a>").attr("href", "#").text("Discounts"));
$("#tabs").append($tabDiscounts);
var $tabBills = $("<li></li>").attr("id", "bills-tab");
$tabBills.append($("<a></a>").attr("href", "#").text("Bills"));
$("#tabs").append($tabBills);

$.getScript("loadFunctions.js");
$("#modals").append($("<div></div").load("modals/NewDiscount.html",
	function() {
		$.getScript("modals/NewDiscount.js");
	}
));
$("#modals").append($("<div></div").load("modals/DayReport.html"));
$("#modals").append($("<div></div").load("modals/DayReportTable.html"));
$("#modals").append($("<div></div").load("modals/CategoryReport.html",
	function() {
		$.getScript("modals/Reports.js");
	}
));
$("#searchPanel").append($("<div></div").load("searchPanel.html",
	function() {
		$.getScript("searchPanel.js");
	}
));

$("#home-tab").click(function(e) {
	e.preventDefault();
	$tab = $(this);
	$tab.parent().children().removeClass("active");
	$tab.attr("class", "active");
	var $main = $("#main");
	$main.empty();
/*	var $tree = $("<div></div>").attr("class", "well");
	$main.append($tree);
	$lst = $("<ul></ul>").attr("class", "nav nav-list").attr("id", "categories-tree");
	$tree.append($lst);
	updateTree();*/
});



$("#showrooms-tab").click(function(e) {
	e.preventDefault();
	$tab = $(this);
	$.get("home", function(data, status){
		$tab.parent().children().removeClass("active");
		$tab.attr("class", "active");
		var $main = $("#main");
		$("#searchPanel").hide();
		$main.removeClass("col-sm-8");
		$main.attr("class", "col-sm-12");
		$main.empty();
		var $table = $("<table></table>");
		$table.attr("class", "table");
		$main.append($table);
		var $header = $("<tr></tr>");
		$header.append($("<th></th>").text("name"));
		$header.append($("<th></th>").text("address"));
		$header.append($("<th></th>").text("telephone"));
		$header.append($("<th></th>").text("e-mail"));
		$header.append($("<th></th>").text("pib"));
		$table.append($header);
		for(var i=0; i<data.length; i++) {
			var $row = $("<tr></tr>");
			$row.append($("<td></td>").text(data[i].name));
			$row.append($("<td></td>").text(data[i].address));
			$row.append($("<td></td>").text(data[i].telephone));
			$row.append($("<td></td>").text(data[i].email));
			$row.append($("<td></td>").text(data[i].pib));
			$table.append($row);
		}
		
	});
	
});

$("#categories-tab").click(function(e) {
	e.preventDefault();
	$tab = $(this);
	$tab.parent().children().removeClass("active");
	$tab.attr("class", "active");
	loadCategories(1);
});

$("#products-tab").click(function(e) {
	e.preventDefault();
	$tab = $(this);
	$tab.parent().children().removeClass("active");
	$tab.attr("class", "active");
	loadProducts(1);
});

$("#services-tab").click(function(e) {
	e.preventDefault();
	$tab = $(this);
	$tab.parent().children().removeClass("active");
	$tab.attr("class", "active");
	loadServices(1);
});

$("#discounts-tab").click(function(e) {
	e.preventDefault();
	$tab = $(this);
	$tab.parent().children().removeClass("active");
	$tab.attr("class", "active");
	loadDiscounts(1);
});

$("#bills-tab").click(function(e) {
	e.preventDefault();
	$tab = $(this);
	$tab.parent().children().removeClass("active");
	$tab.attr("class", "active");
	loadAccounts(1);
});

$("#searchButton").click(function(e) {
	e.preventDefault();
	var txt = $("#searchField").val();

	var $table = $("#main").find("tbody").first();
	var pat = new RegExp(txt, "i");
	if($table) {
		$.each($table.children(), function() {
			var hidden = true;
			$.each($(this).children(), function() {
				if($(this).text().search(pat) >= 0) {
					hidden = false;
				}
				if($(this).prop("tagName") == "TH") {
					hidden = false;
				}
			});
			if(hidden) {
				$(this).hide();
			} else {
				$(this).show();
			}
		});
	}
});