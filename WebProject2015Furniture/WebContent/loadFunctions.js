var loadShowroomsDropDowns = function(user) {
	$.get("home", function(data, status){
		if(user == 0) {
			$("#select-showroom-modal").empty();
			$("#select-showroom-modal-edit").empty();
		}
		$("#select-discount-showroom-modal").empty();
		$("#search-showroom").empty();
		$("#search-showroom").append($("<option></option>").text("-"));
		
		for(var i=0; i<data.length; i++) {
			if(user == 0) {
				$("#select-showroom-modal").append($("<option></option>")
						.text(data[i].name).attr("id", data[i].pib));
				$("#select-showroom-modal-edit").append($("<option></option>")
						.text(data[i].name).attr("id", data[i].pib));
			}
			$("#search-showroom").append($("<option></option>")
					.text(data[i].name).attr("id", data[i].pib));
			$("#select-discount-showroom-modal").append($("<option></option>")
					.text(data[i].name).attr("id", data[i].pib));
		}
	});
};

var loadCategoriesDropDowns = function(user) {
	$.get("categories", function(data) {
		if(user == 0) {
			$("#select-category-modal").empty();
			$("#select-supercategory-modal").empty();
			$("#select-category-modal-edit").empty();
			$("#select-supercategory-modal-edit").empty();
			$("#select-supercategory-modal").append($("<option></option>").text("-"));
			$("#select-supercategory-modal-edit").append($("<option></option>").text("-"));
		}
		
		if(user == 1) {
			$("#select-category-modal-report").empty();
		}
		
		if(user == 0 || user == 1) {
			$("#select-discount-category-modal").empty();
		}
		
		$("#search-category").empty();
		$("#search-category").append($("<option></option>").text("-"));
		
		for(var i=0; i<data.length; i++) {
			if(user == 0) {
				$("#select-category-modal").append($("<option></option>").text(data[i].name));
				$("#select-supercategory-modal").append($("<option></option>").text(data[i].name));
				$("#select-category-modal-edit").append($("<option></option>").text(data[i].name));
				$("#select-supercategory-modal-edit").append($("<option></option>").text(data[i].name));
			}
			
			if(user == 1) {
				$("#select-category-modal-report").append($("<option></option>").text(data[i].name));
			}
			if(user == 0 || user == 1) {
				$("#select-discount-category-modal").append($("<option></option>").text(data[i].name));
			}
			
			$("#search-category").append($("<option></option>").text(data[i].name));
		}
	});
};

var loadProductsDropDowns = function() {
	$.get("items", function(data) {
		$("#select-item-modal").empty();
		$("#select-item-modal-edit").empty();
		$("#select-discount-item-modal").empty();
		for(var i=0; i<data.length; i++) {
			$("#select-item-modal").append($("<option></option>")
					.text(data[i].name).attr("code", data[i].code));
			$("#select-item-modal-edit").append($("<option></option>")
					.text(data[i].name).attr("code", data[i].code));
			$("#select-discount-item-modal").append($("<option></option>")
					.text(data[i].name).attr("code", data[i].code));
		}
	});
}

var isInPast = function(d) {
	var date = new Date(d);
	var nw = new Date();
	
	if(date.getYear() < nw.getYear()) {
		return true;
	} else if(date.getYear() > nw.getYear()) {
		return false;
	} else if(date.getMonth() < nw.getMonth()) {
		return true;
	} else if(date.getMonth() > nw.getMonth()) {
		return false;
	} else if(date.getDate() <= nw.getDate()) {
		return true;
	} else  {
		return false;
	}
}

var isInFuture = function(d) {
	var date = new Date(d);
	var nw = new Date();
	
	if(date.getYear() > nw.getYear()) {
		return true;
	} else if(date.getYear() < nw.getYear()) {
		return false;
	} else if(date.getMonth() > nw.getMonth()) {
		return true;
	} else if(date.getMonth() < nw.getMonth()) {
		return false;
	} else if(date.getDate() >= nw.getDate()) {
		return true;
	} else  {
		return false;
	}
}

var getDiscount = function(item, discounts, categories) {
	var d = 0;
	for(var x in discounts) {
		if(discounts[x].it == 0) {
			if(discounts[x].item.code == item.code) {
				if(isInPast(discounts[x].start) && isInFuture(discounts[x].finish))
				if(discounts[x].percent > d) {
					d = discounts[x].percent;
				}
			}
		}
	}
	var type = item.type;
	while(type) {
		for(var x in discounts) {
			if(discounts[x].it == 1) {
				if(discounts[x].type.name == type.name &&
						item.showroom.pib == discounts[x].showroom.pib) {
					if(isInPast(discounts[x].start) && isInFuture(discounts[x].finish))
					if(discounts[x].percent > d) {
						d = discounts[x].percent;
					}
				}
			}
		}
		if(type.superType) {
			type = categories[type.superType.name];
		} else {
			type = null;
		}
	}
	
	return d;
}


var loadProducts = function(user) {
	loadCategoriesDropDowns(user);
	loadShowroomsDropDowns(user);
	$.get("categories", {name : ""}, function(data, status) {
		if(status != "success") {
			window.location.replace("login.html");
		}
		var cats = data;
		var categories = {};
		for(var i in cats) {
			categories[cats[i].name] = cats[i];
		}
	$.get("discounts", function(data) {
		var discounts = data;
	$.get("items", function(data) {
		var $main = $("#main");
		$main.empty();

		$("#searchPanel").show();
		$(".service-hide").show();
		$main.removeClass("col-sm-12");
		$main.attr("class", "col-sm-10");

		if(user == 0) {
			var $addbtn = $("<button></button>").attr("data-target", "#itemModal")
				.attr("type", "button").attr("data-toggle", "modal").attr("class", "btn btn-primary")
				.text("Add Product");
			$main.append($addbtn);
		}
		var $table = $("<table></table>");
		$table.attr("class", "table");
		$main.append($table);
		var $hrow = $("<tr></tr>");
		$hrow.append($("<th></th>").text("code"));
		$hrow.append($("<th></th>").text("name"));
		$hrow.append($("<th></th>").text("count"));
		$hrow.append($("<th></th>").text("manufacturer"));
		$hrow.append($("<th></th>").text("price"));
		$hrow.append($("<th></th>").text("off"));
		$hrow.append($("<th></th>").text("color"));
		$hrow.append($("<th></th>").text("showroom"));
		$hrow.append($("<th></th>").text("category"));
		$hrow.append($("<th></th>").text("year"));
		$hrow.append($("<th></th>").text("country of origin"));
		$hrow.append($("<th></th>").text("photo"));
		$hrow.append($("<th></th>").text(" "));
		
		var $btnrefresh = $("<a></a>").attr("href", "#");
		$btnrefresh.append($("<span></span>").attr("class","glyphicon glyphicon-refresh"));
		$hrow.append($("<td></td>").append($btnrefresh));
		$btnrefresh.attr("data-toggle", "tooltip").attr("title", "Refresh");
		$btnrefresh.click(function() {loadProducts(user);});
		
		$table.append($hrow);
		$("#select-item-modal").empty();
		$("#select-item-modal-edit").empty();
		for(var i=0; i<data.length; i++) {
			$("#select-item-modal").append($("<option></option>").text(data[i].name).attr("code", data[i].code));
			$("#select-item-modal-edit").append($("<option></option>").text(data[i].name).attr("code", data[i].code));
			var $row = $("<tr></tr>");
			$row.append($("<td></td>").text(data[i].code));
			$row.append($("<td></td>").text(data[i].name));
			$row.append($("<td></td>").text(data[i].count));
			$row.append($("<td></td>").text(data[i].producer));
			$row.append($("<td></td>").text(Number(data[i].price).toFixed(2)));
			$row.append($("<td></td>").text(getDiscount(data[i], discounts, categories) + "%"));
			$row.append($("<td></td>").text(" ").css("background-color", "rgb(" + data[i].color[0] + "," + data[i].color[1] + "," + data[i].color[2] + ")"));
			$row.append($("<td></td>").text(data[i].showroom.name));
			$row.append($("<td></td>").text(data[i].type.name));
			$row.append($("<td></td>").text(data[i].year));
			$row.append($("<td></td>").text(data[i].countryOfOrigin));
			$row.append($("<td></td>").append($("<a></a>").attr("href", "upload/" + data[i].imageOrVideo).append(
					$("<img></img>").attr("src", "upload/" + data[i].imageOrVideo).
					attr("height", "50px"))));
			
			//console.log(data[i].imageOrVideo);
			
			if(user == 0) {
				var $btnedit = $("<a></a>").attr("code", data[i].code).attr("href", "#");
				$btnedit.append($("<a></a>").attr("class","glyphicon glyphicon-edit"));
				$row.append($("<td></td>").append($btnedit));
				$btnedit.click(editClick);
				var $btndelete = $("<a></a>").attr("code", data[i].code).attr("href", "#");
				$btndelete.append($("<span></span>").attr("class","glyphicon glyphicon-remove"));
				$row.append($("<td></td>").append($btndelete));
				$btndelete.click(deleteClick);
			}
			
			if(user == 2) {
				var $btncart = $("<a></a>").attr("code", data[i].code).attr("href", "#");
				$btncart.append($("<span></span>").attr("class","glyphicon glyphicon-shopping-cart").attr("href", "#"));
				$btncart.click(chooseServices);
				$btncart.attr("data-toggle", "tooltip").attr("title", "Add this to shoppingcart");
				$row.append($("<td></td>").append($btncart));
				
				var $btnservices = $("<a></a>").attr("code", data[i].code).attr("href", "#");
				$btnservices.append($("<span></span>").attr("class","glyphicon glyphicon-plus").attr("href", "#"));
				$btnservices.click(showServices);
				$btnservices.attr("data-toggle", "tooltip").attr("title", "Show additional services");
				$row.append($("<td></td>").append($btnservices));
			}
			//$row.append('<td><a data-target="#categoryModal"><span class="glyphicon glyphicon-edit"></span></a></td>');
			//$row.append('<td><a><span class="glyphicon glyphicon-remove"></span></a></td>');
			$table.append($row);
		}
	});
	
	});
	
	});
};

var loadCategories = function(user) {
	loadCategoriesDropDowns(user);
	loadShowroomsDropDowns(user);
	$.get("categories", {name : ""}, function(data, status){
		if(status != "success") {
			window.location.replace("login.html");
		}
		var $main = $("#main");
		$("#searchPanel").hide();
		$main.removeClass("col-sm-10");
		$main.attr("class", "col-sm-12");
		$main.empty();
		if(user == 0) {
			var $addbtn = $("<button></button>").attr("data-target", "#categoryModal")
				.attr("type", "button").attr("data-toggle", "modal").attr("class", "btn btn-primary")
				.text("Add Category");
			$main.append($addbtn);
		}
		var $table = $("<table></table>");
		$table.attr("class", "table");
		$main.append($table);
		var $header = $("<tr></tr>");
		$header.append($("<th></th>").text("name"));
		$header.append($("<th></th>").text("description"));
		$header.append($("<th></th>").text("super category"));
		$header.append($("<th></th>").text(" "));
		$header.append($("<th></th>").text(" "));
		$header.append($("<th></th>").text(" "));
		var $btnrefresh = $("<a></a>").attr("href", "#");
		$btnrefresh.append($("<span></span>").attr("class","glyphicon glyphicon-refresh"));
		$header.append($("<td></td>").append($btnrefresh));
		$btnrefresh.attr("data-toggle", "tooltip").attr("title", "Refresh");
		$btnrefresh.click(function() {loadCategories(user);});
		$table.append($header);
		for(var i=0; i<data.length; i++) {
			var $row = $("<tr></tr>");
			$row.append($("<td></td>").text(data[i].name));
			$row.append($("<td></td>").text(data[i].description));
			if(data[i].superType != null) {
				$row.append($("<td></td>").text(data[i].superType.name));
			} else {
				$row.append($("<td></td>").text("-"));
			}
			
			if(user == 0) {
				var $btnedit = $("<a></a>").attr("name", data[i].name).attr("href", "#");
				$btnedit.append($("<span></span>").attr("class","glyphicon glyphicon-edit"));
				$row.append($("<td></td>").append($btnedit));
				$btnedit.click(editCategoryClick);
				var $btndelete = $("<a></a>").attr("name", data[i].name).attr("href", "#");
				$btndelete.append($("<span></span>").attr("class","glyphicon glyphicon-remove"));
				$row.append($("<td></td>").append($btndelete));
				$btndelete.click(deleteCategoryClick);
			}
			
			$row.append($("<td></td>").text(data[i].email));
			$row.append($("<td></td>").text(data[i].pib));
			$table.append($row);
		}
		
	});
};

var loadServices = function(user) {
	$.get("services", function(data, status){
		if(status != "success") {
			window.location.replace("login.html");
		}
		var $main = $("#main");
		$main.empty();
		
		$main.removeClass("col-sm-12");
		$main.attr("class", "col-sm-10");
		$("#searchPanel").show();
		$(".service-hide").hide();
		
		if(user == 0) {
			var $addbtn = $("<button></button>").attr("data-target", "#serviceModal")
				.attr("type", "button").attr("data-toggle", "modal").attr("class", "btn btn-primary")
				.text("Add Service");
			$main.append($addbtn);
			loadProductsDropDowns();
		}
		var $table = $("<table></table>");
		$table.attr("class", "table");
		$main.append($table);
		var $header = $("<tr></tr>");
		$header.append($("<th></th>").text("name"));
		$header.append($("<th></th>").text("description"));
		$header.append($("<th></th>").text("prices"));
		$header.append($("<th></th>").text("product"));
		$header.append($("<th></th>").text(" "));
		$header.append($("<th></th>").text(" "));
		$header.append($("<th></th>").text(" "));
		var $btnrefresh = $("<a></a>").attr("href", "#");
		$btnrefresh.append($("<span></span>").attr("class","glyphicon glyphicon-refresh"));
		$header.append($("<td></td>").append($btnrefresh));
		$btnrefresh.attr("data-toggle", "tooltip").attr("title", "Refresh");
		$btnrefresh.click(function() {loadServices(user);});
		$table.append($header);
		for(var i=0; i<data.length; i++) {
			var $row = $("<tr></tr>");
			$row.append($("<td></td>").text(data[i].name));
			$row.append($("<td></td>").text(data[i].description));
			if(data[i].price)
				$row.append($("<td></td>").text(Number(data[i].price).toFixed(2)));
			else
				$row.append($("<td></td>").text("FREE").css("color", "green"));
			$row.append($("<td></td>").text(data[i].item.name));

			if(user == 0) {
				var $btnedit = $("<a></a>").attr("name", data[i].name).attr("href", "#");
				$btnedit.append($("<span></span>").attr("class","glyphicon glyphicon-edit"));
				$row.append($("<td></td>").append($btnedit));
				$btnedit.click(editServiceClick);
				var $btndelete = $("<a></a>").attr("name", data[i].name).attr("href", "#");
				$btndelete.append($("<span></span>").attr("class","glyphicon glyphicon-remove"));
				$row.append($("<td></td>").append($btndelete));
				$btndelete.click(deleteServiceClick);
			}
			
			$row.append($("<td></td>").text(data[i].email));
			$row.append($("<td></td>").text(data[i].pib));
			$table.append($row);
		}
		
	});
};

var loadDiscounts = function(user) {
	$.get("discounts", function(data, status){
		if(status != "success") {
			window.location.replace("login.html");
		}
		var $main = $("#main");
		$("#search-panel").remove();
		$main.removeClass("col-sm-10");
		$main.attr("class", "col-sm-12");
		$("#searchPanel").hide();
		$main.empty();
		if(user == 0 || user == 1) {
			loadProductsDropDowns();
			loadCategoriesDropDowns(user);
			loadShowroomsDropDowns(user);
			var $addbtn = $("<button></button>").attr("data-target", "#discountModal")
				.attr("type", "button").attr("data-toggle", "modal").attr("class", "btn btn-primary")
				.text("Add Discount");
			$("#radio0").prop("checked", true)
			$main.append($addbtn);
			loadProductsDropDowns();
		}
		var $table = $("<table></table>");
		$table.attr("class", "table");
		$main.append($table);
		var $header = $("<tr></tr>");
		$header.append($("<th></th>").text("showroom"));
		$header.append($("<th></th>").text("product"));
		$header.append($("<th></th>").text("category"));
		$header.append($("<th></th>").text("percent"));
		$header.append($("<th></th>").text("starts"));
		$header.append($("<th></th>").text("expries"));
		var $btnrefresh = $("<a></a>").attr("href", "#");
		$btnrefresh.append($("<span></span>").attr("class","glyphicon glyphicon-refresh"));
		$header.append($("<td></td>").append($btnrefresh));
		$btnrefresh.attr("data-toggle", "tooltip").attr("title", "Refresh");
		$btnrefresh.click(function() {loadDiscounts(user);});
		$table.append($header);
		for(var i=0; i<data.length; i++) {
			var $row = $("<tr></tr>");
			if(data[i].it == "0") {
				$row.append($("<td></td>").text(data[i].item.showroom.name));
				$row.append($("<td></td>").text(data[i].item.name));
				$row.append($("<td></td>").text("-"));
			} else {
				$row.append($("<td></td>").text(data[i].showroom.name));
				$row.append($("<td></td>").text("-"));
				$row.append($("<td></td>").text(data[i].type.name));
			}
			$row.append($("<td></td>").text(data[i].percent + "%"));
			var start = new Date(data[i].start);
			var finish = new Date(data[i].finish);
			$row.append($("<td></td>").text(start.getDate()+"."+ (start.getMonth()+1) +"."+start.getFullYear()));
			$row.append($("<td></td>").text(finish.getDate()+"."+ (finish.getMonth()+1) +"."+finish.getFullYear()));

			$table.append($row);
		}
		
	});
};

var loadAccounts = function(user) {
	if(user != 1)
		return;
	$.get("bills", function(data, status){
		if(status != "success") {
			window.location.replace("login.html");
		}
		var $main = $("#main");
		
		$main.removeClass("col-sm-10");
		$main.attr("class", "col-sm-12");
		$("#searchPanel").hide();
		$main.empty();
		
		$btnday = $("<button></button>").attr("data-target", "#dayReportModal")
			.attr("type", "button").attr("data-toggle", "modal").attr("class", "btn btn-primary")
			.text("Generate report by day");
		
		$main.append($btnday);
		
		$btncat = $("<button></button>").attr("data-target", "#categoryReportModal")
			.attr("type", "button").attr("data-toggle", "modal").attr("class", "btn btn-primary")
			.text("Generate report by category");
	
		$main.append($btncat);
		
		var $table = $("<table></table>");
		$table.attr("class", "table");
		$main.append($table);
		var $header = $("<tr></tr>");
		$header.append($("<th></th>").text("user"));
		$header.append($("<th></th>").text("price"));
		$header.append($("<th></th>").text("date"));
		$header.append($("<th></th>").text("products"));
		
		var $btnrefresh = $("<a></a>").attr("href", "#");
		$btnrefresh.append($("<span></span>").attr("class","glyphicon glyphicon-refresh"));
		$header.append($("<td></td>").append($btnrefresh));
		$btnrefresh.attr("data-toggle", "tooltip").attr("title", "Refresh");
		$btnrefresh.click(function() {loadAccounts(user);});
		$table.append($header);
		for(var i=0; i<data.length; i++) {
			var $row = $("<tr></tr>");
			var date = new Date(data[i].dateAndTime);
			$row.append($("<td></td>").text(data[i].customer.name));
			$row.append($("<td></td>").text(Number(data[i].totalPrice).toFixed(2)));
			$row.append($("<td></td>").text(date.getDate()+"."+ (date.getMonth()+1)+"."+date.getFullYear()+"  "
					+date.getHours()+":"+date.getMinutes()+":"
					+(date.getSeconds() > 10 ? date.getSeconds() : "0" + date.getSeconds().toString())));
			$row.append($("<td></td>").text(data[i].items.length));


			$table.append($row);
		}
		
	});
};

var loadCart = function(user) {
	if(user != 2)
		return;
	$.get("discounts", function(data, status) {
		if(status != "success") {
			window.location.replace("login.html");
		}
		var discounts = data;
	$.get("shoppingcart", function(data) {
		$("#cart-tab").find("span").text(data.length);
		
		var $main = $("#main");
		$main.removeClass("col-sm-10");
		$main.attr("class", "col-sm-12");
		$("#searchPanel").hide();
		$main.empty();
		var $table = $("<table></table>");
		$table.attr("class", "table");
		$main.append($table);
		var $hrow = $("<tr></tr>");
		$hrow.append($("<th></th>").text("code"));
		$hrow.append($("<th></th>").text("name"));
		$hrow.append($("<th></th>").text("manufacturer"));
		$hrow.append($("<th></th>").text("price"));
		//$hrow.append($("<th></th>").text("off"));
		$hrow.append($("<th></th>").text("showroom"));
		$hrow.append($("<th></th>").text("category"));
		$hrow.append($("<th></th>").text("count"));
		$hrow.append($("<th></th>").text("services"));
		$hrow.append($("<th></th>").text("total"));
		$hrow.append($("<th></th>").text(" "));
		$hrow.append($("<th></th>").text(" "));
		
		var $btnrefresh = $("<a></a>").attr("href", "#");
		$btnrefresh.append($("<span></span>").attr("class","glyphicon glyphicon-refresh"));
		$hrow.append($("<td></td>").append($btnrefresh));
		$btnrefresh.attr("data-toggle", "tooltip").attr("title", "Refresh");
		$btnrefresh.click(function() {loadCart(user);});
		$table.append($hrow);
		for(var i=0; i<data.length; i++) {
			var $row = $("<tr></tr>");
			$row.append($("<td></td>").text(data[i].product.code));
			$row.append($("<td></td>").text(data[i].product.name));
			$row.append($("<td></td>").text(data[i].product.producer));
			$row.append($("<td></td>").text(Number(data[i].product.price).toFixed(2)));
			//$row.append($("<td></td>").text(getDiscount(data[i].product, discounts) + "%"));
			$row.append($("<td></td>").text(data[i].product.showroom.name));
			$row.append($("<td></td>").text(data[i].product.type.name));
			$row.append($("<td></td>").text(data[i].count));
			var $badge = $("<a></a>").append($("<span></span>")
						.attr("class", "badge").text(data[i].services.length));
			$badge.attr("code", data[i].product.code);
			$badge.click(editServices);
			$row.append($("<td></td>").append($badge));
			
			var total = data[i].count * data[i].product.price;
			for(var j in data[i].services) {
				total += data[i].services[j].price;
			}
			
			$row.append($("<td></td>").text(total.toFixed(2)));
			
			var $btncart = $("<a></a>").attr("code", data[i].product.code).attr("href", "#");
			$btncart.append($("<span></span>").attr("class","glyphicon glyphicon-plus"));
			$btncart.click(addProductClick);
			$row.append($("<td></td>").append($btncart));
			
			var $btncart = $("<a></a>").attr("code", data[i].product.code).attr("href", "#");
			$btncart.append($("<span></span>").attr("class","glyphicon glyphicon-minus"));
			$btncart.click(minusProductClick);
			$row.append($("<td></td>").append($btncart));
			
			
			var $btncart = $("<a></a>").attr("code", data[i].product.code).attr("href", "#");
			$btncart.append($("<span></span>").attr("class","glyphicon glyphicon-remove"));
			$btncart.click(removeProductClick);
			$row.append($("<td></td>").append($btncart));
			//$row.append('<td><a data-target="#categoryModal"><span class="glyphicon glyphicon-edit"></span></a></td>');
			//$row.append('<td><a><span class="glyphicon glyphicon-remove"></span></a></td>');
			$table.append($row);
		}
		
		if(data.length) {
			$btnbuy = $("<button></button>").attr("type", "button")
					.attr("class", "btn btn-primary")
					.text("Buy").attr("id", "buy");
		
			$main.append($btnbuy);
			$btnbuy.click(buyClick);
		}
		
	});
	
	});
};
