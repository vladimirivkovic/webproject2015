var showServices = function(e) {
	var code = $(this).attr("code");
	e.preventDefault();
	var $showModal = $("#showServices");
	
	$.get("services", {code: code}, function(data) {
		$showModal.find(".modal-body").empty();
		var $tbl = $("<table></table?").attr("class", "table");

		if(data.length) {
			$showModal.find(".modal-body").append($tbl);
			var $hrow = $("<tr></tr>");
			$hrow.append($("<th></th>").text("name"));
			$hrow.append($("<th></th>").text("description"));
			$hrow.append($("<th></th>").text("price"));
			$tbl.append($hrow);
			for(var x in data) {
				var $row = $("<tr></tr>");
				$row.append($("<td></td>").text(data[x].name));
				$row.append($("<td></td>").text(data[x].description));
				if(data[x].price)
					$row.append($("<td></td>").text(data[x].price));
				else
					$row.append($("<td></td>").text("FREE").css("color", "green"));
				$tbl.append($row);
			}
		} else {
			$showModal.find(".modal-body").append("There is no additional services for this product");
		}

		$showModal.modal();
	});
};