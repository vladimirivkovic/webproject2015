

var editServices = function(e) {
	e.preventDefault();
	var code = $(this).attr("code");

	var $editModal = $("#editServices");
	
	$.get("services", {code: code}, function(data) {
		$editModal.find(".modal-body").empty();
		var $tbl = $("<table></table>").attr("class", "table");

		if(data.length) {
			$.get("shoppingcart", {code: code}, function(datax) {
				var services = {};
				for(var i in datax) {
					services[datax[i].name] = 1;
				}
				$editModal.find(".modal-body").append($tbl);
				var $hrow = $("<tr></tr>");
				$hrow.append($("<th></th>").text("name"));
				$hrow.append($("<th></th>").text("description"));
				$hrow.append($("<th></th>").text("price"));
				$hrow.append($("<th></th>").text("choose"));
				$tbl.append($hrow);
				for(var x in data) {
					var $row = $("<tr></tr>");
					$row.append($("<td></td>").text(data[x].name));
					$row.append($("<td></td>").text(data[x].description));
					$row.append($("<td></td>").text(data[x].price));
					//var $fg = $("<div></div>").attr("class", "checkbox");
					var $chb = $("<input>").attr("type", "checkbox")
							.attr("code", data[x].name);
					if(services[data[x].name]) {
						$chb.prop('checked', true);
					}
					//$fg.append($chb);
					$row.append($("<td></td>").append($chb));
					$tbl.append($row);
				}
			});
		} else {
			$editModal.find(".modal-body").append("There is no additional services for this product");
		}

		$("#edit-services").attr("code", code);
		
		$editModal.modal();
	});
};

