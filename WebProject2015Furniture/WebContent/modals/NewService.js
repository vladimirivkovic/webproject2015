$("#new-service").click(function(e) {
	e.preventDefault();
	var name = $("#service-name").val();
	var desc = $("#service-desc").val();
	var price = $("#service-price").val();
	var code = $("#select-item-modal").find(":selected").attr("code");
	
	if(!validateNewService()) {
		return;
	}
	
	$.post(
			"services",
			{
				code: code,
				name: name,
				desc: desc,
				price: price,
				mod: "+"
			},
			function(data, status) {
				if(status == "success") {
					$("#serviceModal").modal("hide");
					$.toaster({ priority : 'success', title : 'Success', message : 'New service added'});
					loadServices(0);
				} else {
					$.toaster({ priority : 'danger', title : 'Error', message : 'Service cannot be added'});
					loadServices(0);
				}
			});
});

var validateNewService = function() {
	var name = $("#service-name").val();
	if(!name) {
		$.toaster({ priority : 'danger', title : 'Error', message : 'Name is empty'});
		return false;
	}
	var desc = $("#service-desc").val();
	if(!desc) {
		$.toaster({ priority : 'danger', title : 'Error', message : 'Description is empty'});
		return false;
	}
	
	var price = $("#service-price").val();
	if(price == "") {
		$.toaster({ priority : 'danger', title : 'Error', message : 'Price is empty'});
		return false;
	}
	if(isNaN(price)) {
		$.toaster({ priority : 'danger', title : 'Error', message : 'Price must be number'});
		return false;
	} else if( Number(price)  < 0) {
		$.toaster({ priority : 'danger', title : 'Error', message : 'Price must be pisitive'});
		return false;
	}
	
	return true;
}