var editServiceClick = function(e) {
	e.preventDefault();
		var name = $(this).attr("name");
		
		$.get("services", {name: name}, function(data) {
			$("#service-name-edit").val(data.name);
			$("#service-desc-edit").val(data.description);
			$("#service-price-edit").val(data.price);
			var item = $("#select-item-modal-edit").find("option[code='" + data.item.code + "']").text();
			$("#select-item-modal-edit").val(item);
			$("#editServiceModal").modal();
		});
};
	
$("#edit-service").click(function(e) {
	e.preventDefault();
	var name = $("#service-name-edit").val();
	var desc = $("#service-desc-edit").val();
	var code = $("#select-item-modal-edit").find(":selected").attr("code");
	var price = $("#service-price-edit").val();
	
	if(!validateEditService()) {
		return;
	}
	
	$.post(
			"services",
			{
				code: code,
				name: name,
				desc: desc,
				price: price,
				mod: "e"
			},
			function(data, status) {
				if(status == "success") {
					$("#editServiceModal").modal("hide");
					$.toaster({ priority : 'success', title : 'Success', message : 'Service edited'});
					loadServices(0);
				} else {
					$.toaster({ priority : 'danger', title : 'Error', message : 'Service cannot be edited'});
					loadServices(0);
				}
			});
});

var validateEditService = function() {
	var name = $("#service-name-edit").val();
	if(!name) {
		$.toaster({ priority : 'danger', title : 'Error', message : 'Name is empty'});
		return false;
	}
	var desc = $("#service-desc-edit").val();
	if(!desc) {
		$.toaster({ priority : 'danger', title : 'Error', message : 'Description is empty'});
		return false;
	}
	
	var price = $("#service-price-edit").val();
	if(price == "") {
		$.toaster({ priority : 'danger', title : 'Error', message : 'Price is empty'});
		return false;
	}
	if(isNaN(price)) {
		$.toaster({ priority : 'danger', title : 'Error', message : 'Price must be number'});
		return false;
	} else if( Number(price)  < 0) {
		$.toaster({ priority : 'danger', title : 'Error', message : 'Price must be pisitive'});
		return false;
	}
	
	return true;
}