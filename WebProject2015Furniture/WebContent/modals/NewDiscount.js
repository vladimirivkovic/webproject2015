$("#new-discount").click(function(e) {
	e.preventDefault();
	var start = $("#discount-start").val();
	var finish = $("#discount-finish").val();
	var percent = $("#discount-percent").val();
	var item = $("#select-discount-item-modal").find(":selected").attr("code");
	var type = $("#select-discount-category-modal").val();
	var showroom = $("#select-discount-showroom-modal").find(":selected").attr("id");
	var it = 0;
	
	if($('input[name=optradio]:radio:checked').attr("id") == "radio1") {
		//alert($('input[name=optradio]:radio:checked').attr("id"));
		it = 1;
	}
	
	if(!validateNewDiscount()) {
		return;
	}
	
	
	$.post(
			"discounts",
			{
				start: start,
				finish: finish,
				percent: percent,
				it: it,
				item: item,
				type: type,
				showroom: showroom
			},
			function(data, status) {
				if(status == "success") {
					$("#discountModal").modal("hide");
					$.toaster({ priority : 'success', title : 'Success', message : 'New discount added'});
				} else {
					$.toaster({ priority : 'danger', title : 'Error', message : 'Discount cannot be added'});
				}
				loadDiscounts(0);
			});
});

var validateNewDiscount = function() {
	var start = $("#discount-start").val();
	var finish = $("#discount-finish").val();
	var percent = $("#discount-percent").val();
	
	if(!start) {
		$.toaster({ priority : 'danger', title : 'Error', message : 'Start date is empty'});
		return false;
	}
	if(!finish) {
		$.toaster({ priority : 'danger', title : 'Error', message : 'Finish date is empty'});
		return false;
	}
	if(!percent) {
		$.toaster({ priority : 'danger', title : 'Error', message : 'Percent is empty'});
		return false;
	}
	if(isNaN(percent)) {
		$.toaster({ priority : 'danger', title : 'Error', message : 'Percent must be positive integer'});
		return false;
	} else {
		if(percent >= 100) {
			$.toaster({ priority : 'danger', title : 'Error', message : 'Percent must be less than 100%'});
			return false;
		}
		for(var i in percent) {
			if(isNaN(percent[i])) {
				$.toaster({ priority : 'danger', title : 'Error', message : 'Percent must be positive integer'});
				return false;
			}
		}
	}
	
	
	return true;
}