var editCategoryClick = function(e) {
	var name = $(this).attr("name");
	e.preventDefault();
	
	$.get("categories", {name: name}, function(data) {
		$("#type-name-edit").val(data.name);
		$("#type-desc-edit").val(data.description);
		if(data.superType == null) {
			$("#select-supercategory-modal-edit").val("-");
		} else {
			$("#select-supercategory-modal-edit").val(data.superType.name);
		}

		$("#editCategoryModal").modal();
	});
};



$("#edit-category").click(function(e) {
	e.preventDefault();
	var name = $("#type-name-edit").val();
	var desc = $("#type-desc-edit").val();
	var spr = $("#select-supercategory-modal-edit").val();
	
	if(!validateEditCategory()) {
		return;
	}
	
	$.post(
			"categories",
			{name: name, desc: desc, spr: spr, mod: "e"},
			function(data, status) {
				if(status != "success") {
					$.toaster({ priority : 'danger', title : 'Error', message : 'Inappropriate super category'});
				} else {
					$("#editCategoryModal").modal("hide");
					$.toaster({ priority : 'success', title : 'Success', message : 'Category edited'});
				}
				loadCategories(0);
			});
});

var validateEditCategory = function() {
	var name = $("#type-name-edit").val();
	var desc = $("#type-desc-edit").val();
	
	if(!name) {
		$.toaster({ priority : 'danger', title : 'Error', message : 'Name is empty'});
		return false;
	}
	if(name == "-") {
		$.toaster({ priority : 'danger', title : 'Error', message : 'Wrong name'});
		return false;
	}
	if(!desc) {
		$.toaster({ priority : 'danger', title : 'Error', message : 'Description is empty'});
		return false;
	}
	
	return true;
}