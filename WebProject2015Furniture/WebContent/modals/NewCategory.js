$("#new-category").click(function(e) {
	e.preventDefault();
	var name = $("#type-name").val();
	var desc = $("#type-desc").val();
	var spr = $("#select-supercategory-modal").val();
	
	if(!validateNewCategory()) {
		return;
	}
	
	$.post("categories", {name: name, desc: desc, spr: spr, mod: "+"}, function(data, status) {
		if(status == "success") {
			/*var $items = $("#categories-tree").find("li");
			$.each($items, function() {
				var $ch = $(this).children().first();
				if($ch.text() == spr) {
					var $lst = null;
					if($ch.prop("tagName") == "A") {
						$ch = $ch.parent();
						$ch.empty();
						var $lblx = $("<label></label>")
							.attr("class", "tree-toggle nav-header")
							.text(spr);
						$ch.append($lblx);
						$lst = $("<ul></ul>").attr("class", "nav nav-list tree");
						$ch.append($lst);
						$lblx.click(nodeClick);
					} else {
						$lst = $ch.parent().children().last();
					}
					var $item = $("<li></li>");
					$item.append($("<a></a>").attr("href", "#").text(name));
					$lst.append($item);
				}
			});*/
			$("#categoryModal").modal("hide");
			$.toaster({ priority : 'success', title : 'Success', message : 'New Category created'});
		} else {
			$.toaster({ priority : 'danger', title : 'Error', message : 'Category cannot be created'});
		}
		loadCategories(0);
	});

});

var validateNewCategory = function() {
	var name = $("#type-name").val();
	var desc = $("#type-desc").val();
	
	if(name == "") {
		$.toaster({ priority : 'danger', title : 'Error', message : 'Name is empty'});
		return false;
	}
	if(name == "-") {
		$.toaster({ priority : 'danger', title : 'Error', message : 'Wrong name'});
		return false;
	}
	if(desc == "") {
		$.toaster({ priority : 'danger', title : 'Error', message : 'Description is empty'});
		return false;
	}
	
	return true;
}