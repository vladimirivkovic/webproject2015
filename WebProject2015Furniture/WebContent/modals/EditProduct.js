var editClick = function(e) {
	e.preventDefault();
	var code = $(this).attr("code");
	
	$.get("items", {code: code}, function(data) {
		$("#item-code-edit").val(data.code);
		$("#item-name-edit").val(data.name);
		$("#item-count-edit").val(data.count);
		$("#select-showroom-modal-edit").val(data.showroom.name);
		$("#select-category-modal-edit").val(data.type.name);
		
		$("#item-manufac-edit").val(data.producer);
		$("#item-price-edit").val(data.price);
		$("#item-year-edit").val(data.year);
		$("#item-country-edit").val(data.countryOfOrigin);
		
		
		var c = "#";
		c += toHex((data.color[0]-data.color[0]%16)/16);
		c += toHex(data.color[0]%16);
		c += toHex((data.color[1]-data.color[1]%16)/16);
		c += toHex(data.color[1]%16);
		c += toHex((data.color[2]-data.color[2]%16)/16);
		c += toHex(data.color[2]%16);
		console.log(c);
		
		$("#item-color-edit").val(c);
		
		
		$("#editItemModal").modal();
	});
};

var toHex = function(d) {
	switch(d) {
	case 10:
		return 'A';
	case 11:
		return 'B';
	case 12:
		return 'C';
	case 13:
		return 'D';
	case 14:
		return 'E';
	case 15:
		return 'F';
	default:
		return d;
	}
}



$("#edit-item").click(function(e) {
	e.preventDefault();
	var code = $("#item-code-edit").val();
	var name = $("#item-name-edit").val();
	var count = $("#item-count-edit").val();
	var type = $("#select-category-modal-edit").val();
	var showroom = $("#select-showroom-modal-edit").find(":selected").attr("id");
	var files = $("#item-image-edit").prop('files');
	
	var color = $("#item-color-edit").val();
	var country = $("#item-country-edit").val();
	var manufac = $("#item-manufac-edit").val();
	var price = $("#item-price-edit").val();
	var year = $("#item-year-edit").val();
	
	if(!validateEditProduct()) {
		return;
	}
	
	var urlUpload = "upload";
	var jForm = new FormData();
	jForm.append("code", code);
	jForm.append("name", name);
	jForm.append("type", type);
	jForm.append("showroom", showroom);
	jForm.append("count", count);
	jForm.append("mod", "e");
	
	jForm.append("color", color);
	jForm.append("country", country);
	jForm.append("manufac", manufac);
	jForm.append("price", price);
	jForm.append("year", year);
	if(files.length > 0)
			jForm.append("image", files[0]);
	
	$.ajax({
        url: urlUpload,
        type: "POST",
        data: jForm,
        mimeType: "multipart/form-data",
        contentType: false,
        cache: false,
        processData: false,
        success: function (data, textStatus, jqXHR) {
        	$("#editItemModal").modal("hide");
            $("#item-image-edit").val("");
            $.toaster({ priority : 'success', title : 'Success', message : 'Product edited'});
            loadProducts(0);
        },
        error: function (jqXHR, textStatus, errorThrown) {
        	$.toaster({ priority : 'danger', title : 'Error', message : 'Product cannot be edited'});
        }
    });
});


var validateEditProduct = function() {
	var code = $("#item-code-edit").val();
	if(!code) {
		$.toaster({ priority : 'danger', title : 'Error', message : 'Code is empty'});
		return false;
	}
	var name = $("#item-name-edit").val();
	if(!name) {
		$.toaster({ priority : 'danger', title : 'Error', message : 'Name is empty'});
		return false;
	}
	var count = $("#item-count-edit").val();
	if(!count) {
		$.toaster({ priority : 'danger', title : 'Error', message : 'Quantity is empty'});
		return false;
	}
	if(isNaN(count)) {
		$.toaster({ priority : 'danger', title : 'Error', message : 'Quantity must be positive integer'});
		return false;
	} else {
		for(var i in count) {
			if(isNaN(count[i])) {
				$.toaster({ priority : 'danger', title : 'Error', message : 'Quantity must be positive integer'});
				return false;
			}
		}
	}
	/*var files = $("#item-image").prop('files');
	if(files.length == 0) {
		$.toaster({ priority : 'danger', title : 'Error', message : 'Photo is not selected'});
		return false;
	}*/
	
	var country = $("#item-country-edit").val();
	if(!country) {
		$.toaster({ priority : 'danger', title : 'Error', message : 'Country of origin is empty'});
		return false;
	}
	var manufac = $("#item-manufac-edit").val();
	if(!manufac) {
		$.toaster({ priority : 'danger', title : 'Error', message : 'Manufacturer is empty'});
		return false;
	}
	
	var price = $("#item-price-edit").val();
	if(price == "") {
		$.toaster({ priority : 'danger', title : 'Error', message : 'Price is empty'});
		return false;
	}
	if(isNaN(price)) {
		$.toaster({ priority : 'danger', title : 'Error', message : 'Price must be number'});
		return false;
	} else if( Number(price)  < 0) {
		$.toaster({ priority : 'danger', title : 'Error', message : 'Price must be pisitive'});
		return false;
	}
		
	var year = $("#item-year-edit").val();
	if(year == "") {
		$.toaster({ priority : 'danger', title : 'Error', message : 'Year is empty'});
		return false;
	}
	if(isNaN(count)) {
		$.toaster({ priority : 'danger', title : 'Error', message : 'Year must be positive integer'});
		return false;
	} else if(Number(year) > 2015) {
		$.toaster({ priority : 'danger', title : 'Error', message : 'Year cannot be in future'});
		return false;
	} else {
		for(var i in year) {
			if(isNaN(year[i])) {
				$.toaster({ priority : 'danger', title : 'Error', message : 'Year must be positive integer'});
				return false;
			}
		}
	}
	
	
	
	return true;
}