
$("#day-report").click(function(e) {
	e.preventDefault();
	var start = $("#report-start").val();
	var finish = $("#report-finish").val();
	
	$.get("dayreport", {start: start, stop: finish}, function(data) {
		var s = "";
		for(var i in data) {
			s += data[i].showroom.name + ":\n";
			for(var j in data[i].amounts) {
				s += data[i].amounts[j] + " ";
			}
			s += "\n";
		}
		//alert(s);
		
		$("#day-report-main").empty();
		var startDate = new Date(start);
		for(var i in data) {
			
			var t = startDate.getTime();
			var $table = $("<table></table>").attr("class", "table");
			$("#day-report-main").append("Showroom: " + data[i].showroom.name);
			$("#day-report-main").append($table);
			var $hrow = $("<tr></tr>");
			$hrow.append($("<th></th>").text("date"));
			$hrow.append($("<th></th>").text("total for day"));
			$table.append($hrow);
			var total = 0;
			for(var j in data[i].amounts) {
				var $row = $("<tr></tr>");
				$row.append($("<td></td>").text(new Date(t)));
				$row.append($("<td></td>").text(data[i].amounts[j]));
				$table.append($row);
				total += data[i].amounts[j];
				t += 24*60*60*1000;
			}
			$("#day-report-main").append("Total for period: ", total);
		}
		$("#dayReportTableModal").modal();
	});
});

$("#category-report").click(function(e) {
	e.preventDefault();
	var start = $("#report-start-c").val();
	var finish = $("#report-finish-c").val();
	var category = $("#select-category-modal-report").val();
	
	$.get("categoryreport", {start: start, stop: finish, category: category}, function(data) {
		var s = "";
		for(var i in data) {
			s += data[i] + " ";
		}
		alert(s);

	});
});