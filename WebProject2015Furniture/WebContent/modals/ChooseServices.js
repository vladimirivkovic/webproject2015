

var chooseServices = function(e) {
	var code = $(this).attr("code");
	e.preventDefault();

	var $chooseModal = $("#chooseServices");
	
	$.get("services", {code: code}, function(data) {
		$chooseModal.find(".modal-body").empty();
		var $tbl = $("<table></table?").attr("class", "table");

		if(data.length) {
			$chooseModal.find(".modal-body").append($tbl);
			var $hrow = $("<tr></tr>");
			$hrow.append($("<th></th>").text("name"));
			$hrow.append($("<th></th>").text("description"));
			$hrow.append($("<th></th>").text("price"));
			$hrow.append($("<th></th>").text("choose"));
			$tbl.append($hrow);
			for(var x in data) {
				var $row = $("<tr></tr>");
				$row.append($("<td></td>").text(data[x].name));
				$row.append($("<td></td>").text(data[x].description));
				if(data[x].price)
					$row.append($("<td></td>").text(data[x].price));
				else
					$row.append($("<td></td>").text("FREE").css("color", "green"));
				$tbl.append($row);
				//var $fg = $("<div></div>").attr("class", "checkbox");
				var $chb = $("<input>").attr("type", "checkbox").attr("code", data[x].name);
				//$fg.append($chb);
				$row.append($("<td></td>").append($chb));
				$tbl.append($row);
			}
		} else {
			$chooseModal.find(".modal-body").append("There is no additional services for this product");
		}

		$("#choose-service").attr("code", code);
		
		$chooseModal.modal();
	});
};

