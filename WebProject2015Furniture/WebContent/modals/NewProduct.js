$("#new-item").click(function(e) {
	e.preventDefault();
	var code = $("#item-code").val();
	var name = $("#item-name").val();
	var type = $("#select-category-modal").val();
	var showroom = $("#select-showroom-modal").find(":selected").attr("id");
	var count = $("#item-count").val();
	var files = $("#item-image").prop('files');
	
	var color = $("#item-color").val();
	var country = $("#item-country").val();
	var manufac = $("#item-manufac").val();
	var price = $("#item-price").val();
	var year = $("#item-year").val();
	
	/*$.post(
			"items",
			{
				code: code,
				name: name,
				type: type,
				showroom: showroom,
				count: count,
				mod: "+"
			},
			function(data, status) {
				if(status == "success")
					$.toaster({ priority : 'success', title : 'Success', message : 'New Category created'});
				else
					$.toaster({ priority : 'danger', title : 'Error', message : 'Category cannot be created'});
				loadProducts(0);
			});*/
	if(!validateNewProduct()) {
		return;
	}
	
	var urlUpload = "upload";
	var jForm = new FormData();
	jForm.append("code", code);
	jForm.append("name", name);
	jForm.append("type", type);
	jForm.append("showroom", showroom);
	jForm.append("count", count);
	jForm.append("mod", "+");
	
	jForm.append("color", color);
	jForm.append("country", country);
	jForm.append("manufac", manufac);
	jForm.append("price", price);
	jForm.append("year", year);
	if(files.length > 0)
			jForm.append("image", files[0]);
	
	$.ajax({
        url: urlUpload,
        type: "POST",
        data: jForm,
        mimeType: "multipart/form-data",
        contentType: false,
        cache: false,
        processData: false,
        success: function (data, textStatus, jqXHR) {
            $("#itemModal").modal("hide");
            $("#item-image").val("");
            $.toaster({ priority : 'success', title : 'Success', message : 'New product added'});
            loadProducts(0);
        },
        error: function (jqXHR, textStatus, errorThrown) {
        	$.toaster({ priority : 'danger', title : 'Error', message : 'Product cannot be added'});
        }
    });
	
	
});

var validateNewProduct = function() {
	var code = $("#item-code").val();
	if(!code) {
		$.toaster({ priority : 'danger', title : 'Error', message : 'Code is empty'});
		return false;
	}
	var name = $("#item-name").val();
	if(!name) {
		$.toaster({ priority : 'danger', title : 'Error', message : 'Name is empty'});
		return false;
	}
	var count = $("#item-count").val();
	if(!count) {
		$.toaster({ priority : 'danger', title : 'Error', message : 'Quantity is empty'});
		return false;
	}
	if(isNaN(count)) {
		$.toaster({ priority : 'danger', title : 'Error', message : 'Quantity must be positive integer'});
		return false;
	} else {
		for(var i in count) {
			if(isNaN(count[i])) {
				$.toaster({ priority : 'danger', title : 'Error', message : 'Quantity must be positive integer'});
				return false;
			}
		}
	}
	var files = $("#item-image").prop('files');
	if(files.length == 0) {
		$.toaster({ priority : 'danger', title : 'Error', message : 'Photo is not selected'});
		return false;
	}
	
	var country = $("#item-country").val();
	if(!country) {
		$.toaster({ priority : 'danger', title : 'Error', message : 'Country of origin is empty'});
		return false;
	}
	var manufac = $("#item-manufac").val();
	if(!manufac) {
		$.toaster({ priority : 'danger', title : 'Error', message : 'Manufacturer is empty'});
		return false;
	}
	
	var price = $("#item-price").val();
	if(price == "") {
		$.toaster({ priority : 'danger', title : 'Error', message : 'Price is empty'});
		return false;
	}
	if(isNaN(price)) {
		$.toaster({ priority : 'danger', title : 'Error', message : 'Price must be number'});
		return false;
	} else if( Number(price)  < 0) {
		$.toaster({ priority : 'danger', title : 'Error', message : 'Price must be pisitive'});
		return false;
	}
		
	var year = $("#item-year").val();
	if(year == "") {
		$.toaster({ priority : 'danger', title : 'Error', message : 'Year is empty'});
		return false;
	}
	if(isNaN(count)) {
		$.toaster({ priority : 'danger', title : 'Error', message : 'Year must be positive integer'});
		return false;
	} else if(Number(year) > 2015) {
		$.toaster({ priority : 'danger', title : 'Error', message : 'Year cannot be in future'});
		return false;
	} else {
		for(var i in year) {
			if(isNaN(year[i])) {
				$.toaster({ priority : 'danger', title : 'Error', message : 'Year must be positive integer'});
				return false;
			}
		}
	}
	
	
	
	return true;
}