var $tabHome = $("<li></li>").attr("class", "active").attr("id", "home-tab");
$tabHome.append($("<a></a>").attr("href", "#").text("Home"));
$("#tabs").append($tabHome);
var $tabProducts = $("<li></li>").attr("id", "products-tab");
$tabProducts.append($("<a></a>").attr("href", "#").text("Products"));
$("#tabs").append($tabProducts);
var $tabServices = $("<li></li>").attr("id", "services-tab");
$tabServices.append($("<a></a>").attr("href", "#").text("Additional services"));
$("#tabs").append($tabServices);
var $tabDiscounts = $("<li></li>").attr("id", "discounts-tab");
$tabDiscounts.append($("<a></a>").attr("href", "#").text("Discounts"));
$("#tabs").append($tabDiscounts);
var $tabCart = $("<li></li>").attr("id", "cart-tab");
var $cartx = $("<a></a>").attr("href", "#").text("My Shooping Cart  ");
$cartx.append($("<span></span>").attr("class", "badge").text("0"));
$tabCart.append($cartx);
$("#tabs").append($tabCart);

$.getScript("loadFunctions.js");
$.get("shoppingcart", function(data) {
	$("#cart-tab").find("span").text(data.length);
});
$("#searchPanel").append($("<div></div").load("searchPanel.html",
	function() {
		$.getScript("searchPanel.js");
	}
));

$("#modals").append($("<div></div").load("modals/ShowServices.html",
	function() {
		$.getScript("modals/ShowServices.js");
	}
));
$("#modals").append($("<div></div").load("modals/ChooseServices.html",
	function() {
		$("#choose-service").click(newProductClick);
		$.getScript("modals/ChooseServices.js");
	}
));
$("#modals").append($("<div></div").load("modals/EditServices.html",
		function() {
			$("#edit-services").click(editServicesClick);
			$.getScript("modals/EditServices.js");
		}
	));


$("#home-tab").click(function(e) {
	e.preventDefault();
	$tab = $(this);
	$tab.parent().children().removeClass("active");
	$tab.attr("class", "active");
	var $main = $("#main");
	$main.empty();
/*	var $tree = $("<div></div>").attr("class", "well");
	$main.append($tree);
	$lst = $("<ul></ul>").attr("class", "nav nav-list").attr("id", "categories-tree");
	$tree.append($lst);
	updateTree();*/
});
$("#products-tab").click(function(e) {
	e.preventDefault();
	$tab = $(this);
	$tab.parent().children().removeClass("active");
	$tab.attr("class", "active");
	loadProducts(2);
});

$("#services-tab").click(function(e) {
	e.preventDefault();
	$tab = $(this);
	$tab.parent().children().removeClass("active");
	$tab.attr("class", "active");
	loadServices(2);
});

$("#discounts-tab").click(function(e) {
	e.preventDefault();
	$tab = $(this);
	$tab.parent().children().removeClass("active");
	$tab.attr("class", "active");
	loadDiscounts(2);
});

$("#cart-tab").click(function(e) {
	e.preventDefault();
	$tab = $(this);
	$tab.parent().children().removeClass("active");
	$tab.attr("class", "active");
	loadCart(2);
});

$("#searchButton").click(function() {
	var txt = $("#searchField").val();

	var $table = $("#main").find("tbody").first();
	var pat = new RegExp(txt, "i");
	if($table) {
		$.each($table.children(), function() {
			var hidden = true;
			$.each($(this).children(), function() {
				if($(this).text().search(pat) >= 0) {
					hidden = false;
				}
				if($(this).prop("tagName") == "TH") {
					hidden = false;
				}
			});
			if(hidden) {
				$(this).hide();
			} else {
				$(this).show();
			}
		});
	}
});

var newProductClick = function(e) {
	var code = $(this).attr("code");
	console.log(code);
	e.preventDefault();
	
	var services = "";
	$.each($("input:checked"), function() {
		services += $(this).attr("code") + "\n";
	});
	
	$.post("shoppingcart", {code: code, action: "n", services: services}, function(data, status) {

		$.get("shoppingcart", function(datax) {
			$("#cart-tab").find("span").text(datax.length);
		});
		
		console.log(status);
		if(status == "success")
			$.toaster({ priority : 'success', title : 'Success', message : 'Selected item added to cart'});
		else
			$.toaster({ priority : 'danger', title : 'Error', message : 'Selected item cannot be added to cart'});
		
		loadProducts(2);
	});
};

var editServicesClick = function(e) {
	var code = $(this).attr("code");
	e.preventDefault();
	console.log(code);
	
	var services = "";
	$.each($("input:checked"), function() {
		services += $(this).attr("code") + "\n";
	});
	
	$.post("shoppingcart", {code: code, action: "e", services: services}, function(data, status) {
		
		console.log(status);
		if(status == "success")
			$.toaster({ priority : 'success', title : 'Success', message : 'Additional services edited'});
		else
			$.toaster({ priority : 'danger', title : 'Error', message : 'Additional service cannot be edited'});
		
		loadCart(2);
	});
};

var addProductClick = function(e) {
	var code = $(this).attr("code");
	e.preventDefault();
	
	$.post("shoppingcart", {code: code, action: "+"}, function(data) {
		$tab = $("#cart-tab");
		$tab.parent().children().removeClass("active");
		$tab.attr("class", "active");
		loadCart(2);	
	});
};



var removeProductClick = function(e) {
	var code = $(this).attr("code");
	e.preventDefault();
	
	$.post("shoppingcart", {code: code, action: "x"}, function(data) {
		$tab = $("#cart-tab");
		$tab.parent().children().removeClass("active");
		$tab.attr("class", "active");
		loadCart(2);
	});
};

var minusProductClick = function(e) {
	var code = $(this).attr("code");
	e.preventDefault();
	
	$.post("shoppingcart", {code: code, action: "-"}, function(data) {
		$tab = $("#cart-tab");
		$tab.parent().children().removeClass("active");
		$tab.attr("class", "active");
		loadCart(2);
	});
};


var buyClick = function(e) {
	e.preventDefault();
	$.post("bills", function(data, status) {
		if(status == "success") {
			$.toaster({ priority : 'success', title : 'Success', message : 'Bill created. Total amount: ' + data});
		} else {
			$.toaster({ priority : 'danger', title : 'Error', message : 'Too much quantity for some products'});
		}
		loadCart(2);
	});
}


